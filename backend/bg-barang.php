<?php
include_once 'koneksi.php';


$search = 0;
if(isset($_GET['search'])){
    $search = $_GET['search'];
}
$opsi = null;
if(isset($_GET['opsi'])){
    $opsi=$_GET['opsi'];
}
if(isset($_GET["op"]))
{
    $opsi = $_GET["op"];
}
   $gr=$_GET['gr'];

   $tabel = "m_master_barang";
    $query_filter = "";
    $data_filter = "";
    $kolom = "";
    $kolom_sl_product = " kode_barang,nama_barang,kode_model,satuan,AKTIF, ";
    $kolom_m_master = " kode_barang,nama_barang,ket,satuan,aktif_tidak, ";

    $orderby = "";
    $orderby_sl_product = " kode_barang ";
    $orderby_m_master = " kode_barang ";

   if($gr==0){
       $title = "Data Barang Jadi";
       $group ='FG';
       $model ='Lap. Data Barang Jadi';
       $tabel = "SL_PRODUCT";
       $query_filter = " WHERE FG_WIP_SCRAP = ";
       $data_filter = "'FG'";
       $kolom = $kolom_sl_product;
       $orderby = $orderby_sl_product;
   } elseif($gr==1) {
        $title = "Data Bahan Baku";
       $group='material';
       $model='Lap. Data Bahan Baku';
       $tabel = "M_MASTER_BARANG";
       $query_filter = " WHERE GROUP_WH_JENIS = ";
       $data_filter = "'$group'";
       $kolom = $kolom_m_master;
       $orderby = $orderby_m_master;

   } elseif($gr==2) {
       $title = "Data Bahan Penolong";
       $group='penolong';
       $model='Lap. Data Bahan Penolong';
       $tabel = "M_MASTER_BARANG";
       $query_filter = " WHERE GROUP_WH_JENIS = ";
       $data_filter = "'$group'";
       $kolom = $kolom_m_master;
       $orderby = $orderby_m_master;

   } elseif($gr==3) {
       $title = "Data Barang Consumable";
       $group='consumable';
       $model='Lap. Barang Consumable';
       $tabel = "M_MASTER_BARANG";
       $query_filter = " WHERE GROUP_WH_JENIS = ";
       $data_filter = "'$group'";
       $kolom = $kolom_m_master;
       $orderby = $orderby_m_master;

   } elseif($gr==4) {
       $title = "Data Barang Modal";
       $group='modal';
       $model='Lap. Barang Modal';
       $tabel = "M_MASTER_BARANG";
       $query_filter = " WHERE GROUP_WH_JENIS = ";
       $data_filter = "'$group'";
       $kolom = $kolom_m_master;
       $orderby = $orderby_m_master;

   } else{
       $title = "Data Barang Scrapt";
       $group='scrap';
       $model='Lap. Barang Scrapt';
       $tabel = "SL_PRODUCT";
       $query_filter = " WHERE FG_WIP_SCRAP = ";
       $data_filter = "'$group'";
       $kolom = $kolom_sl_product;
       $orderby = $orderby_sl_product;

   }

   $strKeyword = null;

    if(isset($_GET["nama"])) $strKeyword = trim($_GET["nama"]);
    if(isset($_GET["cr"])) $strKeyword = trim($_GET["cr"]);
    
    $kode = null;

    if(isset($_GET["kode"])) $kode = trim($_GET["kode"]);
    
    if(isset($_GET["kd"])) $kode = trim($_GET["kd"]);

    if(isset($_GET['hal']) && $_GET['hal'] !== "")
    {   
        $noPage = $_GET['hal'];
    } 
    else $noPage = 1;

    //$dataPerPage = 25;
    $offset = ($noPage - 1) * $dataPerPage;
    
    
    
    if(isset($_GET['setpage']) && $_GET['setpage'] !== "")
    {   
        $paging = $_GET['setpage'];
    } 

    if(!isset($paging) || $paging !== "0"){
        $paging = " where T.rownumb between (($noPage-1)*$dataPerPage)+1 and ($noPage*$dataPerPage) ";
    }else{
        $paging = '';
    }
    
    if($opsi == 0){ //Filter By : Semua Data
        $strQuery = "SELECT * from 
        (select $kolom
        row_number() over (order by kode_barang) as rownumb 
        from $tabel $query_filter $data_filter) T
        $paging 
        order by $orderby";
        $str_count = "SELECT COUNT(kode_barang) FROM $tabel $query_filter $data_filter";
        $query   = $db->query($str_count);

    } elseif($opsi==1){ // Filter By : Kode Barang

        $strQuery = "SELECT * from 
        (select $kolom
        row_number() over (order by kode_barang) as rownumb 
        from $tabel $query_filter $data_filter and kode_barang like '%$kode%') T 
        $paging
        order by $orderby";
        $str_count = "SELECT COUNT(kode_barang) FROM $tabel 
        $query_filter $data_filter and kode_barang like '%$kode%'";
        $query   = $db->query($str_count);
       
    } else { // Filter By : Nama Barang
        $strQuery = "SELECT * from 
        (select $kolom
        row_number() over (order by kode_barang) as rownumb 
        from $tabel $query_filter $data_filter and nama_barang like '%$strKeyword%') T 
        $paging
        order by $orderby";

        $str_count = "SELECT COUNT(kode_barang) FROM $tabel $query_filter $data_filter and nama_barang like'%$strKeyword%'";
        $query   = $db->query($str_count);
    }



   $result=$db->query($strQuery);
   $dataq   = $query->fetch();
   $jumData = $dataq[0];
