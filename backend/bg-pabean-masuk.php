<?php
    include_once 'koneksi.php';
    $title = "Data Pabean Masuk";

    $search = 0;
    if(isset($_GET['search'])){
        $search = $_GET['search'];
    }
    if(isset($_GET['opsi'])){
        $opsi=$_GET['opsi'];
    }
    if(isset($_GET["op"]))
    {
        $opsi = $_GET["op"];
    }

    if(isset($_GET['tgl1'])){
        $tgl1=$_GET['tgl1'];
    }
   
    if(isset($_GET['tgl2'])){
        $tgl2=$_GET['tgl2'];
    }
  
    $nbc = null;
    if(isset($_GET['nomer'])){
        $nbc=$_GET['nomer'];
    }
    if(isset($_GET['nbc'])){
        $nbc=$_GET['nbc'];
    }
   

    $jbc=null;
    if(isset($_GET['jenis'])){
        $jbc=$_GET['jenis'];
    }
    if(isset($_GET["jbc"]))
    {
        $jbc = $_GET["jbc"];
    }


    $kode = null;
    if(isset($_GET["kode"]))
    {
        $kode = $_GET["kode"];
    }
    if(isset($_GET["kdx"]))
    {
        $kode = $_GET["kdx"];
    }


    if(isset($_GET['hal']) && $_GET['hal'] !== "")
    {
        $noPage = $_GET['hal'];
    } else $noPage = 1;

    if ($tgl1=='' or $tgl2=='') {
        $tgl1='1800-01-01';
        $tgl2='1800-01-01';
    } else {
        $tgl1;
        $tgl2;
    }
    //$dataPerPage = 25;
    $offset = ($noPage - 1) * $dataPerPage;

    if(isset($_GET['setpage']) && $_GET['setpage'] !== "")
    {   
        $paging = $_GET['setpage'];
    } 

    if(!isset($paging) || $paging !== "0"){
        $paging = " where T.rownumb between (($noPage-1)*$dataPerPage)+1 and ($noPage*$dataPerPage) ";
    }else{
        $paging = '';
    }

    try {
        $ip=$_SERVER['REMOTE_ADDR'];
        $query_main = "SELECT * FROM (
            SELECT b2_in.bc_jenis,b2_in.bc_nomer,
            replace(convert(varchar, b2_in.bc_tgl, 103), '/','-') as bc_tgl,
            b2_in.no_do,
            replace(convert(varchar, b2_in.tgl_mutasi_aktual, 103), '/','-') as tgl_mutasi_aktual, 
            m_supplier_customer.nama_supp_cust,
            b2_in.d_kode_barang,m_master_barang.nama_barang,b2_in.d_unit, 
            b2_in.d_jumlah,b2_in.d_mata_uang, CAST(b2_in.d_subtotal AS DECIMAL(14,2)) as d_subtotal,
            row_number() over (order by b2_in.bc_nomer) as rownumb
            from b2_in 
            left join m_master_barang ON b2_in.d_kode_barang = m_master_barang.kode_barang 
            left join m_supplier_customer on b2_in.kode_supp_cust=m_supplier_customer.kode_supp_cust ";

        if($opsi==0){
            $query_main .= "WHERE b2_in.tgl 
                between '$tgl1' AND '$tgl2' AND b2_in.sumber_penerimaan_beacukai_umum ='BEACUKAI' 
            ) T
             $paging 
            order by bc_tgl,bc_jenis,bc_nomer asc";
            $result=$db->query($query_main);


            $query   = $db->query("SELECT COUNT(b2_in.bc_nomer) FROM b2_in 
            left join m_master_barang ON b2_in.d_kode_barang = m_master_barang.kode_barang 
            left join m_supplier_customer on b2_in.kode_supp_cust=m_supplier_customer.kode_supp_cust
            where b2_in.tgl >='$tgl1' AND b2_in.tgl <='$tgl2' AND b2_in.sumber_penerimaan_beacukai_umum ='BEACUKAI'
            ");
            $dataq   = $query->fetch();
            $jumData = $dataq[0];


        } elseif($opsi==1){
            $query_main .= "WHERE b2_in.d_kode_barang like '%$kode%'
            AND (b2_in.tgl >='$tgl1' 
            AND b2_in.tgl <='$tgl2') 
            AND b2_in.sumber_penerimaan_beacukai_umum ='BEACUKAI' ) T
            $paging 
            order by bc_tgl,bc_jenis,bc_nomer asc";
            $result=$db->query($query_main);

            $query   = $db->query("SELECT COUNT(b2_in.bc_nomer) 
            FROM b2_in 
            left join m_master_barang ON b2_in.d_kode_barang = m_master_barang.kode_barang 
            left join m_supplier_customer on b2_in.kode_supp_cust=m_supplier_customer.kode_supp_cust
            where b2_in.tgl >='$tgl1' AND b2_in.tgl <='$tgl2' 
            AND b2_in.sumber_penerimaan_beacukai_umum ='BEACUKAI' and d_kode_barang like '%$kode%'");
            $dataq   = $query->fetch();
            $jumData = $dataq[0];
        } elseif($opsi==2){
            $query_main .= "
            WHERE b2_in.bc_nomer like '%$nbc%' 
            AND (b2_in.tgl >='$tgl1' 
            AND b2_in.tgl <='$tgl2') 
            AND b2_in.sumber_penerimaan_beacukai_umum ='BEACUKAI' ) T
            $paging 
            order by bc_tgl,bc_jenis,bc_nomer asc";
            // echo $query_main;die;
            $result=$db->query($query_main);

            $query   = $db->query("SELECT COUNT(b2_in.bc_nomer) 
            FROM b2_in 
            left join m_master_barang ON b2_in.d_kode_barang = m_master_barang.kode_barang 
            left join m_supplier_customer on b2_in.kode_supp_cust=m_supplier_customer.kode_supp_cust
            where b2_in.tgl >='$tgl1' AND b2_in.tgl <='$tgl2' 
            AND b2_in.sumber_penerimaan_beacukai_umum ='BEACUKAI' and bc_nomer like '%$nbc%'");
            $dataq   = $query->fetch();
            $jumData = $dataq[0];
        } else {
            $query_main .= "
            WHERE b2_in.bc_jenis LIKE '%$jbc%' 
            AND (b2_in.tgl between '$tgl1' AND '$tgl2')
            AND b2_in.sumber_penerimaan_beacukai_umum ='BEACUKAI' ) T
            $paging 
            order by bc_tgl,bc_jenis,bc_nomer asc";

            $result=$db->query($query_main);

            $query   = $db->query("SELECT COUNT(b2_in.bc_nomer) 
            FROM b2_in 
            left join m_master_barang ON b2_in.d_kode_barang = m_master_barang.kode_barang 
            left join m_supplier_customer on b2_in.kode_supp_cust=m_supplier_customer.kode_supp_cust
            where b2_in.tgl >='$tgl1' AND b2_in.tgl <='$tgl2' 
            AND b2_in.sumber_penerimaan_beacukai_umum ='BEACUKAI' and bc_jenis like '%$jbc%'");
            $dataq   = $query->fetch();
            $jumData = $dataq[0];
        }
        
        $da1=date_format(date_create($tgl1),"d/M/Y");
        $da2=date_format(date_create($tgl2),"d/M/Y");
    }
    catch (PDOException $e) {
        print "Koneksi atau query bermasalah: " . $e->getMessage() . "<br/>";
        die();
     }
?>