<?php

include_once 'koneksi.php';

$title = "Data Hasil Produksi";
$tgl1 = null;
$tgl2 = null;
$kode = null;
$nama = null;

$search = 0;
if (isset($_GET['search'])) {
 $search = $_GET['search'];
}
$opsi = null;
if (isset($_GET['opsi'])) {
 $opsi = $_GET['opsi'];
}

$strKeyword = null;

if (isset($_GET["tgl1"])) {
 $tgl1 = $_GET["tgl1"];
}

if (isset($_GET["tgl2"])) {
 $tgl2 = $_GET["tgl2"];
}

// if ($tgl1=='' or $tgl2=='') {
//     $tgl1='1800-01-01';
//     $tgl2='1800-01-01';
// } else {
//     $tgl1;
//     $tgl2;
// }

if (isset($_GET["nama"])) {
 $strKeyword = $_GET["nama"];
}

if (isset($_GET["kode"])) {
 $kode = $_GET["kode"];
}

if (isset($_GET['hal']) && $_GET['hal'] !== "") {
 $noPage = $_GET['hal'];
} else {
 $noPage = 1;
}

//$dataPerPage = 25;
$offset = ($noPage - 1) * $dataPerPage;

if (isset($_GET['setpage']) && $_GET['setpage'] !== "") {
 $paging = $_GET['setpage'];
}

if (!isset($paging) || $paging !== "0") {
 $paging = " where T.rownumb between (($noPage-1)*$dataPerPage)+1 and ($noPage*$dataPerPage) ";
} else {
 $paging = '';
}

try {
    $ip = $_SERVER['REMOTE_ADDR'];

    $query_hasprod = " SELECT * FROM (SELECT replace(convert(varchar, tgl, 103), '/','-') as tgl, d_kode_barang, d_nama_barang, d_unit, wip_lokasi,
    lokasi_barang_internal, d_jumlah,
    row_number() over (order by d_kode_barang, d_nama_barang) as rownumb
    FROM WIP_IN ";

 if ($opsi == 0) {
    $query_hasprod .= "
        WHERE tgl BETWEEN '$tgl1' AND '$tgl2') T
        $paging
        ORDER BY tgl, d_kode_barang, d_nama_barang ASC ";
        // echo $query_hasprod;die;
    $result = $db->query($query_hasprod);

    $query = $db->query("SELECT COUNT(*) FROM WIP_IN
            WHERE tgl BETWEEN '$tgl1' AND '$tgl2'");
    $dataq = $query->fetch();
    $jumData = $dataq[0];

 } else if ($opsi == 1) {
    $query_hasprod .= "
        WHERE d_kode_barang LIKE '%$kode%'
        AND tgl BETWEEN '$tgl1' AND '$tgl2') T
        $paging
        ORDER BY tgl, d_kode_barang,d_nama_barang ASC";
    $result = $db->query($query_hasprod);

    $query = $db->query("SELECT COUNT(*) FROM WIP_IN
        WHERE  d_kode_barang LIKE '%$kode%' AND tgl BETWEEN '$tgl1' AND '$tgl2'");
    $dataq = $query->fetch();
    $jumData = $dataq[0];

 } else if ($opsi == 2) {
    $query_hasprod .= "
        WHERE d_nama_barang LIKE '%$strKeyword%' 
        AND tgl BETWEEN '$tgl1' AND '$tgl2') T
        $paging
        ORDER BY tgl, d_kode_barang,d_nama_barang ASC";
    $result = $db->query($query_hasprod);

    $query = $db->query("SELECT COUNT(*) FROM WIP_IN
        WHERE d_nama_barang LIKE '%$strKeyword%' AND tgl BETWEEN '$tgl1' AND '$tgl2'");
    $dataq = $query->fetch();
    $jumData = $dataq[0];
 }

} catch (PDOException $e) {
    print "Koneksi atau query bermasalah: " . $e->getMessage() . "<br/>";
    die();
}
