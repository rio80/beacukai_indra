<?php
    $view = function ($filename, $title = '') {
    session_start();
    $_SESSION['section_header'] = $title;
    $root_dir = "pages/";
    include_once $root_dir . 'page-header.php';
    include_once 'pages/modal/modal.php';

?>


<div class="card">
    <div id="loading" class="text-center"><i class="fas fa-cog fa-spin"></i></div>
    <style>
        .fa-cog {
            font-size: 80px;
        }
    </style>

</div>

<script src="assets/modules/jquery.min.js"></script>


<?php
require_once $root_dir . $filename;
include_once $root_dir . 'page-footer.php';
};