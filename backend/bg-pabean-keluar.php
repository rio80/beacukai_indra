<?php
include_once 'koneksi.php';
$title = "Data Pabean Keluar";

$search = 0;
if (isset($_GET['search'])) {
    $search = $_GET['search'];
}
if (isset($_GET['opsi'])) {
    $opsi = $_GET['opsi'];
}
if (isset($_GET["op"])) {
    $opsi = $_GET["op"];
}

if (isset($_GET['tgl1'])) {
    $tgl1 = $_GET['tgl1'];
}

if (isset($_GET['tgl2'])) {
    $tgl2 = $_GET['tgl2'];
}

$nbc = null;
if (isset($_GET['nomer'])) {
    $nbc = $_GET['nomer'];
}
if (isset($_GET['nbc'])) {
    $nbc = $_GET['nbc'];
}

$jbc = null;
if (isset($_GET['jenis'])) {
    $jbc = $_GET['jenis'];
}
if (isset($_GET["jbc"])) {
    $jbc = $_GET["jbc"];
}

$kode = null;
if (isset($_GET["kode"])) {
    $kode = $_GET["kode"];
}
if (isset($_GET["kdx"])) {
    $kode = $_GET["kdx"];
}

if (isset($_GET['hal']) && $_GET['hal'] !== "") {
    $noPage = $_GET['hal'];
} else {
    $noPage = 1;
}

if ($tgl1 == '' or $tgl2 == '') {
    $tgl1 = '1800-01-01';
    $tgl2 = '1800-01-01';
} else {
    $tgl1;
    $tgl2;
}
//$dataPerPage = 25;
$offset = ($noPage - 1) * $dataPerPage;

if (isset($_GET['setpage']) && $_GET['setpage'] !== "") {
    $paging = $_GET['setpage'];
}

if (!isset($paging) || $paging !== "0") {
    $paging = " where T.rownumb between (($noPage-1)*$dataPerPage)+1 and ($noPage*$dataPerPage) ";
} else {
    $paging = '';
}
try {
    $ip = $_SERVER['REMOTE_ADDR'];
    $query_pabout = " SELECT * FROM (
        SELECT h.bc_jenis, h.bc_nomer,replace(convert(varchar,  h.bc_tgl, 103), '/','-') as bc_tgl,
         h.no_do,replace(convert(varchar, h.tgl, 103), '/','-') as tgl,h.nama_pt,
         d.KODE_BARANG, d.nama_barang,d.satuan,
         d.qty, d.mata_uang, CAST(d.subtotal AS DECIMAL(14,2)) as d_subtotal,
         row_number() over (order by h.bc_nomer) as rownumb
         FROM sl_do_h h
         LEFT JOIN sl_do_d d ON h.NO_DO = d.NO_DO_D ";
    if ($opsi == 0) {
        $query_pabout .= "
        WHERE h.tgl BETWEEN '$tgl1' AND '$tgl2'
        AND h.PENGELUARAN_BEACUKAI_UMUM = 'BEACUKAI'
        AND h.DOKUMEN_DO = 'DO') T
        $paging
        ORDER BY bc_tgl,bc_jenis,bc_nomer ASC";
        $result = $db->query($query_pabout);

        $query = $db->query("SELECT COUNT(h.bc_nomer) 
        FROM sl_do_h h 
        LEFT JOIN sl_do_d d ON h.NO_DO = d.NO_DO_D 
        where h.tgl 
        between '$tgl1' AND '$tgl2' 
        AND PENGELUARAN_BEACUKAI_UMUM = 'BEACUKAI'
        AND DOKUMEN_DO = 'DO'");
        $dataq = $query->fetch();
        $jumData = $dataq[0];
        
    } elseif ($opsi == 1) {
        $query_pabout .= "
        WHERE d.KODE_BARANG LIKE '%$kode%'
        AND h.tgl BETWEEN '$tgl1' AND '$tgl2'
        AND h.PENGELUARAN_BEACUKAI_UMUM = 'BEACUKAI'
        AND h.DOKUMEN_DO = 'DO') T
        $paging
        ORDER BY tgl, bc_jenis, bc_nomer";

        $result = $db->query($query_pabout);

        $query = $db->query("SELECT COUNT(h.bc_nomer)
        FROM sl_do_h h
        LEFT JOIN sl_do_d d ON h.NO_DO = d.NO_DO_D 
        where h.tgl
        between '$tgl1' AND '$tgl2'
        AND PENGELUARAN_BEACUKAI_UMUM = 'BEACUKAI'
        AND h.DOKUMEN_DO = 'DO' and d.kode_barang like '%$kode%'");
        $dataq = $query->fetch();
        $jumData = $dataq[0];

    } elseif ($opsi == 2) {
        $query_pabout .= "
        WHERE h.bc_nomer LIKE '%$nbc%'
        AND h.tgl BETWEEN '$tgl1' AND '$tgl2'
        AND h.PENGELUARAN_BEACUKAI_UMUM = 'BEACUKAI'
        AND h.DOKUMEN_DO = 'DO') T
        $paging
        ORDER BY tgl, bc_jenis, bc_nomer";

        $result = $db->query($query_pabout);

        $query = $db->query("SELECT COUNT(h.bc_nomer)
        FROM sl_do_h h
        LEFT JOIN sl_do_d d ON h.NO_DO = d.NO_DO_D 
        where h.tgl
        between '$tgl1' AND '$tgl2'
        AND PENGELUARAN_BEACUKAI_UMUM = 'BEACUKAI'
        AND h.DOKUMEN_DO = 'DO' and h.bc_nomer like '%$nbc%'");
        $dataq = $query->fetch();
        $jumData = $dataq[0];
    } else {
        $query_pabout .= "
        WHERE h.bc_jenis LIKE '%$jbc%'
        AND h.tgl BETWEEN '$tgl1' AND '$tgl2'
        AND h.PENGELUARAN_BEACUKAI_UMUM = 'BEACUKAI'
        AND h.DOKUMEN_DO = 'DO' ) T
        $paging
        ORDER BY h.tgl, h.bc_jenis, h.bc_nomer";

        $result = $db->query($query_pabout);

        $query = $db->query("SELECT COUNT(h.bc_nomer)
        FROM sl_do_h h
        LEFT JOIN sl_do_d d ON h.NO_DO = d.NO_DO_D 
         where h.tgl
         between '$tgl1' AND '$tgl2'
         AND h.PENGELUARAN_BEACUKAI_UMUM = 'BEACUKAI'
        AND h.DOKUMEN_DO = 'DO' AND h.bc_jenis LIKE '%$jbc%'");
        $dataq = $query->fetch();
        $jumData = $dataq[0];
    }
    $da1 = date_format(date_create($tgl1), "d/M/Y");
    $da2 = date_format(date_create($tgl2), "d/M/Y");
} catch (PDOException $e) {
    print "Koneksi atau query bermasalah: " . $e->getMessage() . "<br/>";
    die();
}
