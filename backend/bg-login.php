<?php
include_once 'koneksi.php';
include_once '../function.php';
include_once 'string/message.php';
include_once 'string/query.php';

$pdo_type = PDO::FETCH_ASSOC;

$username = addslashes($_POST['userbc']);
$password = addslashes($_POST['passbc']);

$as_row = 'as_row';
$table = 'user_password';

// $query_login berasal dari backend/string/query.php
$query_cek_user = $query_login('cek_username', array($as_row, $table, $username));
$query_cek_pass = $query_login('cek_password', array($table, $password));
$cek_user = $db->query($query_cek_user);

$tmp_cek_user = $cek_user->fetch($pdo_type)[$as_row];

//Lanjut cek password jika username exist
$message_empty = null;
if(empty($username)) $message_empty = ("Username ");
if(empty($password)) $message_empty .= (!empty($message_empty) ? "dan Password " : "Password ");
$message_empty .= "Kosong";

if(empty($username) || empty($password)){
  echo "<script>alert('$message_empty');window.history.back();</script>";exit;
}


if ($tmp_cek_user > 0) {
 $cek_pass = $db->query($query_cek_pass);
 $row_fetch = $cek_pass->fetch($pdo_type);

 $cek_row = $row_fetch['nama_user'];
 $cek_bisa_lihat = $row_fetch['web_bisa_lihat'];

 // $message_login berasal dari backend/string/message.php

 if (!empty($cek_row)) {
  if ($cek_bisa_lihat !== 'YA') {
   echo $message_login('na');
  } else {
//    echo $message_login('sc');
   
   $_SESSION['userbc'] = $username;
   $qwaktu = $db->query("SELECT getdate()");
   $waktu = $qwaktu->fetch();
   #$qsavelog=$db->query("INSERT INTO loginmhs(npm,waktu,ip,etc) values('$username','$waktu[0]','$ip','$etc')") or die(mssql_error());
   login_validate();
   header("location:../menupilih.php?pg=100");
  }
 } else {
  echo $message_login('wp');
 }
} else {
 echo $message_login('wu');
}
