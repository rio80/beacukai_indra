<?php
    include_once 'koneksi.php';

    $title = "Data Mutasi Bulanan";
    $bulan = null;
    $tahun = null;
    $kode = null;
    $nama = null;

    $search = 0;
    if(isset($_GET['search'])){
        $search = $_GET['search'];
    }

    if (isset($_GET['opsi'])) {
        $opsi = $_GET['opsi'];
    }

    if (isset($_GET['op'])) {
        $opsi = $_GET['op'];
    }

    if (isset($_GET['bulan'])) {
        $bulan = $_GET['bulan'];
    }

    if (isset($_GET['tahun'])) {
        $tahun = $_GET['tahun'];
    }

    if (isset($_GET['kode'])) {
        $kode = $_GET['kode'];
    }

    if (isset($_GET['nama'])) {
        $nama = $_GET['nama'];
    }

    $gr=$_GET['gr'];

    $stok_wh = "stok_wh";
    $stok_fg = "stok_fg";

    if($gr==0){
        $title = "Data Mutasi Barang Jadi";
        $group='FG';
        $model='Lap. Data Barang Jadi';
        $table = $stok_fg;
    } elseif($gr==1) {
        $title = "Data Mutasi Bahan Baku";
        $group='material';
        $model='Lap. Data Bahan Baku';
        $table = $stok_wh;
    } elseif($gr==2) {
        $title = "Data Mutasi Bahan Penolong";
        $group='penolong';
        $model='Lap. Data Bahan Penolong';
        $table = $stok_wh;

    } elseif($gr==3) {
        $title = "Data Mutasi Barang Consumable";
        $group='consumable';
        $model='Lap. Barang Consumable';
        $table = $stok_wh;

    } elseif($gr==4) {
        $title = "Data Mutasi Barang Modal";
        $group='modal';
        $model='Lap. Barang Modal';
        $table = $stok_wh;

    } else{
        $title = "Data Mutasi Barang Scrapt";
        $group='SCRAP';
        $model='Lap. Barang Scrapt';
        $table = $stok_fg;

    }

    if (isset($_GET['hal']) && $_GET['hal'] !== "") {
        $noPage = $_GET['hal'];
    } else {
        $noPage = 1;
    }
    
    //$dataPerPage = 10;
    $offset = ($noPage - 1) * $dataPerPage;
    
    if (isset($_GET['setpage']) && $_GET['setpage'] !== "") {
        $paging = $_GET['setpage'];
    }
    
    if (!isset($paging) || $paging !== "0") {
        $paging = " where T.rownumb between (($noPage-1)*$dataPerPage)+1 and ($noPage*$dataPerPage) ";
    } else {
        $paging = '';
    }

    try {
        $ip = $_SERVER['REMOTE_ADDR'];
        $query_mutbulan = " SELECT * FROM (
            SELECT kode_barang, nama_barang, satuan, 
            awal, total_in, total_out, adj_tambah, stok, opname,
            selisih, ket,
            row_number() over (order by kode_barang) as rownumb
            FROM $table WHERE kategori_input = '$group' AND ";
        if ($opsi == 0) {
            $query_mutbulan .= "
             tahun_stok = '$tahun' AND bulan_stok = '$bulan') T
            $paging
            ORDER BY kode_barang,nama_barang ASC";
            $result = $db->query($query_mutbulan);
    
            $query = $db->query("SELECT COUNT(*) FROM $table WHERE kategori_input = '$group'
            AND tahun_stok = '$tahun' AND bulan_stok = '$bulan'");
            $dataq = $query->fetch();
            $jumData = $dataq[0];
            
        } elseif ($opsi == 1) {
            $query_mutbulan .= "
             kode_barang LIKE '%$kode%'
            AND tahun_stok = '$tahun' AND bulan_stok = '$bulan') T
            $paging
            ORDER BY kode_barang,nama_barang ASC";
    
            $result = $db->query($query_mutbulan);
    
            $query = $db->query("SELECT COUNT(*) FROM $table
            WHERE kategori_input = '$group'
            AND kode_barang = '$kode' AND tahun_stok = '$tahun' AND bulan_stok = '$bulan'");
            $dataq = $query->fetch();
            $jumData = $dataq[0];
    
        } elseif ($opsi == 2) {
            $query_mutbulan .= "
            nama_barang LIKE '%$nama%'
            AND tahun_stok = '$tahun' AND bulan_stok = '$bulan') T
            $paging
            ORDER BY kode_barang,nama_barang ASC";
    
            $result = $db->query($query_mutbulan);
    
            $query = $db->query("SELECT COUNT(*) FROM $table
            WHERE kategori_input = '$group'
            AND nama_barang = '$nama' AND tahun_stok = '$tahun' AND bulan_stok = '$bulan'");

            $dataq = $query->fetch();
            $jumData = $dataq[0];
        }
    } catch (PDOException $e) {
        print "Koneksi atau query bermasalah: " . $e->getMessage() . "<br/>";
        die();
    }
?>