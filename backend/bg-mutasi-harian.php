<?php
include_once 'koneksi.php';

$title = "Data Mutasi Harian";
$tgl1 = null;
$tgl2 = null;
$kode = null;
$nama = null;
$bulan = null;
$tahun = null;
$data_bulanan = null;

$gr = 0;
$opsi = 0;

$search = 0;

if (isset($_GET['search'])) {
    $search = $_GET['search'];
}

if (isset($_GET['opsi'])) {
    $opsi = $_GET['opsi'];
}

if (isset($_GET['op'])) {
    $opsi = $_GET['op'];
}

if (isset($_GET['tgl1'])) {
    $tgl1 = $_GET['tgl1'];
}

if (isset($_GET['tgl2'])) {
    $tgl2 = $_GET['tgl2'];
}

if (isset($_GET['kode'])) {
    $kode = $_GET['kode'];
}

if (isset($_GET['nama'])) {
    $nama = $_GET['nama'];
}


$gr=$_GET['gr'];


$stok_wh = "stok_wh";
$stok_fg = "stok_fg";

$stok_wh_data = "stok_wh_data";
$stok_fg_data = "stok_fg_data";

// $tgl1 = "12-11-2019";
// $tgl2 = "19-11-2019";

$get_last = date("Y-m-t", strtotime($tgl2));
$last_date = date('d', strtotime($get_last));

$tgl1_date = date('d', strtotime($tgl1));
$tgl1_m = date('m', strtotime($tgl1));
$tgl1_y = date('Y', strtotime($tgl1));

$tgl2_date = date('d', strtotime($tgl2));
$tgl2_m = date('m', strtotime($tgl2));
$tgl2_y = date('Y', strtotime($tgl2));

$tgl1 = $tgl1_y . "-" . $tgl1_m . "-" . $tgl1_date;
$tgl2 = $tgl2_y . "-" . $tgl2_m . "-" . $tgl2_date;

// Jika dipilih tgl1 dengan tanggal 1 dan tgl2 dengan tanggal akhir bulan
if (($last_date === $tgl2_date) && $tgl1_date == 1 && ($tgl1_m == $tgl2_m)) {
    $data_bulanan = true;
    $bulan = date('m', strtotime($tgl1));
    $tahun = date('Y', strtotime($tgl1));
}

if ($gr == 0) {
    $title = "Data Mutasi Barang Jadi";
    $group = 'FG';
    $model = 'Lap. Data Barang Jadi';
    $table_barang = 'SL_PRODUCT';
    $table = $stok_fg_data;
    if ($data_bulanan) {
        $table = $stok_fg;
    }

} elseif ($gr == 1) {
    $title = "Data Mutasi Bahan Baku";
    $group = 'MATERIAL';
    $model = 'Lap. Data Bahan Baku';
    $table_barang = 'm_master_barang';
    $table = $stok_wh_data;
    if ($data_bulanan) {
        $table = $stok_wh;
    }

} elseif ($gr == 2) {
    $title = "Data Mutasi Bahan Penolong";
    $group = 'PENOLONG';
    $model = 'Lap. Data Bahan Penolong';
    $table_barang = 'm_master_barang';
    $table = $stok_wh_data;
    if ($data_bulanan) {
        $table = $stok_wh;
    }

} elseif ($gr == 3) {
    $title = "Data Mutasi Barang Consumable";
    $group = 'CONSUMABLE';
    $model = 'Lap. Barang Consumable';
    $table_barang = 'm_master_barang';
    $table = $stok_wh_data;
    if ($data_bulanan) {
        $table = $stok_wh;
    }

} elseif ($gr == 4) {
    $title = "Data Mutasi Barang Modal";
    $group = 'MODAL';
    $model = 'Lap. Barang Modal';
    $table_barang = 'm_master_barang';
    $table = $stok_wh_data;
    if ($data_bulanan) {
        $table = $stok_wh;
    }

} else {
    $title = "Data Mutasi Barang Scrapt";
    $group = 'SCRAP';
    $model = 'Lap. Barang Scrapt';
    $table_barang = 'SL_PRODUCT';
    $table = $stok_fg_data;
    if ($data_bulanan) {
        $table = $stok_fg;
    }

}
session_start();
// $user = $_SESSION['userbc'];

if (isset($_GET['hal']) && $_GET['hal'] !== "") {
    $noPage = $_GET['hal'];
} else {
    $noPage = 1;
}

//$dataPerPage = 10;
$offset = ($noPage - 1) * $dataPerPage;

if (isset($_GET['setpage']) && $_GET['setpage'] !== "") {
    $paging = $_GET['setpage'];
}

if (!isset($paging) || $paging !== "0") {
    $paging = " where rownumb between (($noPage-1)*$dataPerPage)+1 and ($noPage*$dataPerPage) ";
} else {
    $paging = '';
}

$temp_mutasi = 'temp_mutasi';

try {
    $ip = $_SERVER['REMOTE_ADDR'];
    $crtbl = $db->query("IF NOT EXISTS (SELECT * from sysobjects where name='$temp_mutasi' and xtype='U') CREATE TABLE $temp_mutasi(kode varchar(50) not null,nama varchar(100),satuan varchar(20),saldoa varchar(20),masuk int,keluar int,adjt int,saldob varchar(20),stok varchar(20), selisih varchar(20), keterangan varchar(20))");
    $cindex = $db->query("IF NOT EXISTS (SELECT name from sys.indexes
            WHERE name = 'mutasi_index') CREATE UNIQUE INDEX mutasi_index on $temp_mutasi(kode) with (IGNORE_DUP_KEY=ON)");
    $del = $db->query("DELETE FROM $temp_mutasi");

    
    $insert_into = "INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,saldob,stok, selisih, keterangan)";

    if ($data_bulanan) {
        
        $pesan_bulanan = "Untuk Periode 1 Bulan Penuh<br>";
        $pesan_bulanan.= "Silahkan Buka Menu Mutasi per Bulan";

        // echo "<script>alert('Harap Buka Menu Mutasi Bulanan')</script>";
        // $query_mut = " SELECT * FROM (
        //         SELECT kode_barang, nama_barang, satuan,
        //         awal, total_in, total_out, adj_tambah, adj, opname,
        //         selisih, ket,
        //         row_number() over (order by kode_barang) as rownumb
        //         FROM $table 
        //         WHERE tahun_stok = '$tahun' 
        //         AND kategori_input = '$group'
        //         AND bulan_stok = '$bulan') T $paging ORDER BY kode_barang,nama_barang ASC";

                
        // $get_data = $db->query($query_mut); 
        // while ($get = $get_data->fetch()) {
        //     $ins1 = $db->query("$insert_into values('$get[0]','$get[1]','$get[2]','$get[3]',$get[4],$get[5],$get[6],'$get[7]','$get[8]','$get[9]','$get[10]') ");

        // }

    } else {
      
        if ($opsi == 0) {

            $all_data_in_1 = "SELECT f.kode_barang, s.nama_barang, s.SATUAN, sum(f.jumlah) AS TOTAL
                FROM $table f
                INNER JOIN $table_barang s
                ON f.kode_barang = s.KODE_BARANG
                WHERE f.in_out_adj_opname ='IN'
                AND kategori_beacukai = '$group'
                AND f.tgl BETWEEN '$tgl1' AND '$tgl2'
                GROUP by f.kode_barang, s.nama_barang, s.SATUAN
                ORDER BY f.kode_barang";

            $get_data_1 = $db->query($all_data_in_1);
            $i = 0;

            while ($get = $get_data_1->fetch()) {
                $qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$get[0]'");
                $cek1 = $qcek1->fetch();
                if ($cek1[0] < 1) {
                $ins1 = $db->query("$insert_into values('$get[0]','$get[1]','$get[2]','',$get[3],0,0,'','','','') ");
                  
                } else {
                    $up1 = $db->query("UPDATE $temp_mutasi set masuk=$get[3] where kode='$get[0]'");
                }

            }

            $all_data_in_2 = "SELECT f.kode_barang, s.nama_barang, s.SATUAN, sum(f.jumlah) AS TOTAL
                FROM $table f
                INNER JOIN $table_barang s
                ON f.kode_barang = s.KODE_BARANG
                WHERE f.in_out_adj_opname ='OUT'
                AND kategori_beacukai = '$group'
                AND f.tgl BETWEEN '$tgl1' AND '$tgl2'
                GROUP by f.kode_barang, s.nama_barang, s.SATUAN
                ORDER BY f.kode_barang";

            $get_data_2 = $db->query($all_data_in_2);

            while ($get = $get_data_2->fetch()) {
                $qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$get[0]'");
                $cek1 = $qcek1->fetch();
                if ($cek1[0] < 1) {
                    $ins1 = $db->query("$insert_into values('$get[0]','$get[1]','$get[2]','',0,$get[3],0,'','','','') ");
                    
                } else {
                    $up1 = $db->query("UPDATE $temp_mutasi set keluar=$get[3] where kode='$get[0]'");
                }

            }

            $all_data_in_3 = "SELECT f.kode_barang, s.nama_barang, s.SATUAN, sum(f.jumlah) AS TOTAL
                FROM $table f
                INNER JOIN $table_barang s
                ON f.kode_barang = s.KODE_BARANG
                WHERE f.in_out_adj_opname ='ADJ'
                AND kategori_beacukai = '$group'
                AND f.tgl BETWEEN '$tgl1' AND '$tgl2'
                GROUP by f.kode_barang, s.nama_barang, s.SATUAN
                ORDER BY f.kode_barang";

            $get_data_3 = $db->query($all_data_in_3);

            while ($get = $get_data_3->fetch()) {
                $qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$get[0]'");
                $cek1 = $qcek1->fetch();
                if ($cek1[0] < 1) {
                    $ins1 = $db->query("$insert_into values('$get[0]','$get[1]','$get[2]','',0,0,$get[3],'','','','') ");
                   
                } else {
                    $up1 = $db->query("UPDATE $temp_mutasi set adjt=$get[3] where kode='$get[0]'");
                }


            }
        } elseif ($opsi == 1) {

            $all_data_in_1 = "SELECT f.kode_barang, s.nama_barang, s.SATUAN, sum(f.jumlah) AS TOTAL
                FROM $table f
                INNER JOIN $table_barang s
                ON f.kode_barang = s.KODE_BARANG
                WHERE f.in_out_adj_opname ='IN'
                AND kategori_beacukai = '$group'
                AND f.kode_barang LIKE '%$kode%'
                AND f.tgl BETWEEN '$tgl1' AND '$tgl2'
                GROUP by f.kode_barang, s.nama_barang, s.SATUAN
                ORDER BY f.kode_barang";

            $get_data_1 = $db->query($all_data_in_1);
            $i = 0;

            while ($get = $get_data_1->fetch()) {
                $qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$get[0]'");
                $cek1 = $qcek1->fetch();
                if ($cek1[0] < 1) {
                    $ins1 = $db->query("$insert_into values('$get[0]','$get[1]','$get[2]','',$get[3],0,0,'','','','') ");
                   
                } else {
                    $up1 = $db->query("UPDATE $temp_mutasi set masuk=$get[3] where kode='$get[0]'");
                }

            }

            $all_data_in_2 = "SELECT f.kode_barang, s.nama_barang, s.SATUAN, sum(f.jumlah) AS TOTAL
                FROM $table f
                INNER JOIN $table_barang s
                ON f.kode_barang = s.KODE_BARANG
                WHERE f.in_out_adj_opname ='OUT'
                AND kategori_beacukai = '$group'
                AND f.kode_barang LIKE '%$kode%'
                AND f.tgl BETWEEN '$tgl1' AND '$tgl2'
                GROUP by f.kode_barang, s.nama_barang, s.SATUAN
                ORDER BY f.kode_barang";

            $get_data_2 = $db->query($all_data_in_2);

            while ($get = $get_data_2->fetch()) {
                $qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$get[0]'");
                $cek1 = $qcek1->fetch();
                if ($cek1[0] < 1) {
                   
                    $ins1 = $db->query("$insert_into values('$get[0]','$get[1]','$get[2]','',0,$get[3],0,'','','','') ");
                } else {
                    $up1 = $db->query("UPDATE $temp_mutasi set keluar=$get[3] where kode='$get[0]'");
                }


            }

            $all_data_in_3 = "SELECT f.kode_barang, s.nama_barang, s.SATUAN, sum(f.jumlah) AS TOTAL
                FROM $table f
                INNER JOIN $table_barang s
                ON f.kode_barang = s.KODE_BARANG
                WHERE f.in_out_adj_opname ='ADJ'
                AND kategori_beacukai = '$group'
                AND s.kode_barang LIKE '%$kode%'
                AND f.tgl BETWEEN '$tgl1' AND '$tgl2'
                GROUP by f.kode_barang, s.nama_barang, s.SATUAN
                ORDER BY f.kode_barang";

            $get_data_3 = $db->query($all_data_in_3);

            while ($get = $get_data_3->fetch()) {
                $qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$get[0]'");
                $cek1 = $qcek1->fetch();
                if ($cek1[0] < 1) {
                    
                    $ins1 = $db->query("$insert_into values('$get[0]','$get[1]','$get[2]','',0,0,$get[3],'','','','') ");
                } else {
                    $up1 = $db->query("UPDATE $temp_mutasi set adjt=$get[3] where kode='$get[0]'");
                }


            }
        } elseif ($opsi == 2) {
            $all_data_in_1 = "SELECT f.kode_barang, s.nama_barang, s.SATUAN, sum(f.jumlah) AS TOTAL
            FROM $table f
            INNER JOIN $table_barang s
            ON f.kode_barang = s.KODE_BARANG
            WHERE f.in_out_adj_opname ='IN'
            AND kategori_beacukai = '$group'
            AND s.nama_barang LIKE '%$nama%'
            AND f.tgl BETWEEN '$tgl1' AND '$tgl2'
            GROUP by f.kode_barang, s.nama_barang, s.SATUAN
            ORDER BY f.kode_barang";

        $get_data_1 = $db->query($all_data_in_1);
        $i = 0;

        while ($get = $get_data_1->fetch()) {
            $qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$get[0]'");
            $cek1 = $qcek1->fetch();
            if ($cek1[0] < 1) {
            $ins1 = $db->query("$insert_into values('$get[0]','$get[1]','$get[2]','',$get[3],0,0,'','','','') ");
                
            } else {
                $up1 = $db->query("UPDATE $temp_mutasi set masuk=$get[3] where kode='$get[0]'");
            }

        }

        $all_data_in_2 = "SELECT f.kode_barang, s.nama_barang, s.SATUAN, sum(f.jumlah) AS TOTAL
            FROM $table f
            INNER JOIN $table_barang s
            ON f.kode_barang = s.KODE_BARANG
            WHERE f.in_out_adj_opname ='OUT'
            AND kategori_beacukai = '$group'
            AND s.nama_barang LIKE '%$nama%'
            AND f.tgl BETWEEN '$tgl1' AND '$tgl2'
            GROUP by f.kode_barang, s.nama_barang, s.SATUAN
            ORDER BY f.kode_barang";

        $get_data_2 = $db->query($all_data_in_2);

        while ($get = $get_data_2->fetch()) {
            $qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$get[0]'");
            $cek1 = $qcek1->fetch();
            if ($cek1[0] < 1) {
            $ins1 = $db->query("$insert_into values('$get[0]','$get[1]','$get[2]','',0,$get[3],0,'','','','') ");
               
            } else {
                $up1 = $db->query("UPDATE $temp_mutasi set keluar=$get[3] where kode='$get[0]'");
            }


        }

        $all_data_in_3 = "SELECT f.kode_barang, s.nama_barang, s.SATUAN, sum(f.jumlah) AS TOTAL
            FROM $table f
            INNER JOIN $table_barang s
            ON f.kode_barang = s.KODE_BARANG
            WHERE f.in_out_adj_opname ='ADJ'
            AND kategori_beacukai = '$group'
            AND s.nama_barang LIKE '%$nama%'
            AND f.tgl BETWEEN '$tgl1' AND '$tgl2'
            GROUP by f.kode_barang, s.nama_barang, s.SATUAN
            ORDER BY f.kode_barang";

        $get_data_3 = $db->query($all_data_in_3);

            while ($get = $get_data_3->fetch()) {
$qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$get[0]'");
                $cek1 = $qcek1->fetch();
                if ($cek1[0] < 1) {
                    $ins1 = $db->query("$insert_into values('$get[0]','$get[1]','$get[2]','',0,0,$get[3],'','','','') ");
                    
                } else {
                    $up1 = $db->query("UPDATE $temp_mutasi set adjt=$get[3] where kode='$get[0]'");
                }

            }
        } 
    }

    $result = $db->query("SELECT * FROM
    (SELECT *,row_number() over (order by kode) as rownumb From $temp_mutasi) T
    $paging order by satuan,kode asc");
    $query = $db->query("SELECT COUNT(kode) FROM $temp_mutasi");
    $dataq = $query->fetch();
    $jumData = $dataq[0];

    // while ($r = $result->fetch()) {
        //     echo "<pre>";
        //     print_r($r);
        // }

} catch (\Throwable $th) {
    //throw $th;
}
