<?php

include_once 'koneksi.php';

$title = "Data Mutasi";

$search = 0;
if(isset($_GET['search'])){
    $search = $_GET['search'];
}

if (isset($_GET['opsi'])) {
    $opsi = $_GET['opsi'];
}
if (isset($_GET["op"])) {
    $opsi = $_GET["op"];
}
if (isset($_GET['tgl1'])) {
    $tgl1 = $_GET['tgl1'];
}
if (isset($_GET["tg"])) {
    $tgl1 = $_GET["tg"];
}
if (isset($_GET['tgl2'])) {
    $tgl2 = $_GET['tgl2'];
}
if (isset($_GET["tg2"])) {
    $tgl2 = $_GET["tg2"];
}
if (isset($_GET['nomer'])) {
    $nbc = $_GET['nomer'];
}
if (isset($_GET["nbc"])) {
    $nbc = $_GET["nbc"];
}
if (isset($_GET['jenis'])) {
    $jbc = $_GET['jenis'];
}
if (isset($_GET["jbc"])) {
    $jbc = $_GET["jbc"];
}

$strKeyword = null;

if (isset($_GET["nama"])) {
    $strKeyword = $_GET["nama"];
}
if (isset($_GET["cr"])) {
    $strKeyword = $_GET["cr"];
}
$kode = null;
if (isset($_GET["kode"])) {
    $kode = $_GET["kode"];
}
if (isset($_GET["kd"])) {
    $kode = $_GET["kd"];
}

$nama = null;
if (isset($_GET["nama"])) {
    $nama = $_GET["nama"];
}

$gr = null;
if (isset($_GET["gr"])) {
    $gr = $_GET["gr"];
}
if (isset($_GET["gr"])) {
    $gr = $_GET["gr"];
}
if ($gr == 0) {
    $group = 'fgwip';
    $model = 'Mutasi Data Barang Jadi';
} elseif ($gr == 1) {
    $group = 'material';
    $model = 'Mutasi Data Bahan Baku';
} elseif ($gr == 2) {
    $group = 'penolong';
    $model = 'Mutasi Data Bahan Penolong';
} elseif ($gr == 3) {
    $group = 'modal';
    $model = 'Mutasi Barang Modal';
} else {
    $group = 'scrapt';
    $model = 'Mutasi Barang Scrapt';
}

if ($tgl1 == '' or $tgl2 == '') {
    $th1 = 0;
    $th2 = 0;
    $bl1 = 0;
    $bl2 = 0;
    $tgl1 = '1800-01-01';
    $tgl2 = '1800-01-01';
} else {
    $t1 = date_create($tgl1);
    $bl = date_format($t1, "m");
    if ($bl == 1) {
        $th1 = date_format($t1, "Y") - 1;
        $bl1 = 12;
    } else {
        $th1 = date_format($t1, "Y");
        $bl1 = date_format($t1, "m") - 1;
    }
    $t2 = date_create($tgl2);
    $th2 = date_format($t2, "Y");
    $bl2 = date_format($t2, "m");
    $d1 = date_format(date_create($tgl1), "M/Y");
    $d2 = date_format(date_create($tgl2), "M/Y");
    $da1 = date_format(date_create($tgl1), "d/M/Y");
    $da2 = date_format(date_create($tgl2), "d/M/Y");
}
if(isset($_GET['hal']) && $_GET['hal'] !== "")
    {   
        $noPage = $_GET['hal'];
    } 
    else $noPage = 1;

    //$dataPerPage = 25;
    $offset = ($noPage - 1) * $dataPerPage;
    
    
    
    if(isset($_GET['setpage']) && $_GET['setpage'] !== "")
    {   
        $paging = $_GET['setpage'];
    } 

    if(!isset($paging) || $paging !== "0"){
        $paging = " where T.rownumb between (($noPage-1)*$dataPerPage)+1 and ($noPage*$dataPerPage) ";
    }else{
        $paging = '';
    }

session_start();

$user = $_SESSION['userbc'];
$temp_mutasi = 'temp_mutasi_' . $user;
try {
    $crtbl = $db->query("IF NOT EXISTS (SELECT * from sysobjects where name='$temp_mutasi' and xtype='U') CREATE TABLE $temp_mutasi(kode varchar(50) not null,nama varchar(100),satuan varchar(20),saldoa int,masuk int,keluar int,adjt int,adjk int,saldob int,stok int)");
    $cindex = $db->query("IF NOT EXISTS (SELECT name from sys.indexes
           WHERE name = 'mutasi_index') CREATE UNIQUE INDEX mutasi_index on $temp_mutasi(kode) with (IGNORE_DUP_KEY=ON)");
    $del = $db->query("DELETE FROM $temp_mutasi");
    if ($opsi == 0) {
        $qproc = $db->query("SELECT b2_opname.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan,Sum(b2_opname.d_jumlah) AS t_jumlah From b2_opname left join m_master_barang on b2_opname.d_kode_barang=m_master_barang.kode_barang Where b2_opname.tahun_stok=$th1 And b2_opname.bulan_stok= $bl1 And b2_opname.kategori_input ='$group' and b2_opname.jenis_data_in_out ='LOCAL' GROUP BY b2_opname.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan order by b2_opname.d_kode_barang asc");
        while ($proc = $qproc->fetch()) {
            $qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$proc[0]'");
            $cek1 = $qcek1->fetch();
            if ($cek1[0] < 1) {
                $ins1 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc[0]','$proc[1]','$proc[2]',$proc[3],0,0,0,0,0,0)");
            } else {
                $up1 = $db->query("UPDATE $temp_mutasi set saldoa=$proc[3] where kode='$proc[0]'");
            }
        }
        $qproc2 = $db->query("SELECT b2_opname.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan,Sum(b2_opname.d_jumlah) AS t_jumlah From b2_opname left join m_master_barang on b2_opname.d_kode_barang=m_master_barang.kode_barang Where b2_opname.tahun_stok=$th2 And b2_opname.bulan_stok= $bl2 And b2_opname.kategori_input ='$group' and b2_opname.jenis_data_in_out ='LOCAL' GROUP BY b2_opname.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan order by b2_opname.d_kode_barang asc");
        while ($proc2 = $qproc2->fetch()) {
            $qcek2 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$proc2[0]'");
            $cek2 = $qcek2->fetch();
            if ($cek2[0] < 1) {
                $ins2 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc2[0]','$proc2[1]','$proc2[2]',0,0,0,0,0,0,$proc2[3]) ");
            } else {
                $up2 = $db->query("UPDATE $temp_mutasi set stok=$proc2[3] where kode='$proc2[0]'");
            }
        }
        $qproc3 = $db->query("SELECT b2_adj.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan,Sum(b2_adj.d_jumlah) as t_jumlah,Sum(b2_adj.d_jumlah_2) as t_jumlah_2 From b2_adj left join m_master_barang on b2_adj.d_kode_barang=m_master_barang.kode_barang where b2_adj.kategori_input ='$group' and b2_adj.tgl_mutasi_aktual >='$tgl1' and b2_adj.tgl_mutasi_aktual <='$tgl2' and b2_adj.jenis_data_in_out ='LOCAL' GROUP BY b2_adj.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan ORDER BY b2_adj.d_kode_barang asc");
        while ($proc3 = $qproc3->fetch()) {
            $qcek3 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$proc3[0]'");
            $cek3 = $qcek3->fetch();
            if ($cek3[0] < 1) {
                $ins3 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc3[0]','$proc3[1]','$proc3[2]',0,0,0,$proc3[3],$proc3[4],0,0)");
            } else {
                $up3 = $db->query("UPDATE $temp_mutasi set adjt=$proc3[3],adjk=$proc3[4] where kode='$proc3[0]'");
            }
        }
        $qproc4 = $db->query("SELECT b2_in.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan, Sum(b2_in.d_jumlah) AS t_jumlah From b2_in left join m_master_barang on b2_in.d_kode_barang=m_master_barang.kode_barang where (b2_in.tgl_mutasi_aktual) between '$tgl1' and '$tgl2' and (b2_in.kategori_input) = '$group' And (b2_in.pabean_tidak) = 'PABEAN' And (b2_in.subcont_tidak) = 'TIDAK' GROUP BY b2_in.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan ORDER BY b2_in.d_kode_barang asc");
        while ($proc4 = $qproc4->fetch()) {
            $qcek4 = $db->query("SELECT count(kode),masuk from $temp_mutasi where kode='$proc4[0]' group by kode,masuk");
            $cek4 = $qcek4->fetch();
            if ($cek4[0] < 1) {
                $ins4 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc4[0]','$proc4[1]','$proc4[2]',0,$proc4[3],0,0,0,0,0)");
            } else {
                $up4 = $db->query("UPDATE $temp_mutasi set masuk=($proc4[3]+$cek4[1]) where kode='$proc4[0]'");
            }
        }
        $qproc41 = $db->query("SELECT b2_in.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan, Sum(b2_in.d_jumlah) AS t_jumlah From b2_in left join m_master_barang on b2_in.d_kode_barang=m_master_barang.kode_barang where (b2_in.tgl_mutasi_aktual) between '$tgl1' and '$tgl2' and (b2_in.kategori_input) = '$group' And (b2_in.local_tidak) = 'LOCAL' GROUP BY b2_in.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan ORDER BY b2_in.d_kode_barang asc");
        while ($proc41 = $qproc41->fetch()) {
            $qcek41 = $db->query("SELECT count(kode),masuk from $temp_mutasi where kode='$proc41[0]' group by kode,masuk");
            $cek41 = $qcek41->fetch();
            if ($cek41[0] < 1) {
                $ins41 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc41[0]','$proc41[1]','$proc41[2]',0,$proc41[3],0,0,0,0,0)");
            } else {
                $up41 = $db->query("UPDATE $temp_mutasi set masuk=($proc41[3]+$cek41[1]) where kode='$proc41[0]'");
            }
        }
        $qproc5 = $db->query("SELECT b2_o.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan, Sum(b2_o.d_jumlah) AS t_jumlah From b2_o left join m_master_barang on b2_o.d_kode_barang=m_master_barang.kode_barang where (b2_o.tgl_mutasi_aktual) between '$tgl1' and '$tgl2' and (b2_o.kategori_input) = '$group' And (b2_o.local_tidak) = 'LOCAL' GROUP BY b2_o.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan ORDER BY b2_o.d_kode_barang asc");
        while ($proc5 = $qproc5->fetch()) {
            $qcek5 = $db->query("SELECT count(kode),keluar from $temp_mutasi where kode='$proc5[0]' group by kode,keluar");
            $cek5 = $qcek5->fetch();
            if ($cek5[0] < 1) {
                $ins5 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc5[0]','$proc5[1]','$proc5[2]',0,0,$proc5[3],0,0,0,0)");
            } else {
                $up5 = $db->query("UPDATE $temp_mutasi set keluar=($proc5[3]+$cek5[1]) where kode='$proc5[0]'");
            }
        }
        $qproc51 = $db->query("SELECT b2_o.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan, Sum(b2_o.d_jumlah) AS t_jumlah From b2_o left join m_master_barang on b2_o.d_kode_barang=m_master_barang.kode_barang where (b2_o.tgl_mutasi_aktual) between '$tgl1' and '$tgl2' and (b2_o.kategori_input) = '$group' And (b2_o.pabean_tidak) = 'PABEAN' And (b2_o.subcont_tidak) = 'TIDAK' GROUP BY b2_o.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan ORDER BY b2_o.d_kode_barang asc");
        while ($proc51 = $qproc51->fetch()) {
            $qcek51 = $db->query("SELECT count(kode),keluar from $temp_mutasi where kode='$proc51[0]' group by kode,keluar");
            $cek51 = $qcek51->fetch();
            if ($cek51[0] < 1) {
                $ins51 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc51[0]','$proc51[1]','$proc51[2]',0,0,$proc51[3],0,0,0,0)");
            } else {
                $up51 = $db->query("UPDATE $temp_mutasi set keluar=($proc51[3]+$cek51[1]) where kode='$proc51[0]'");
            }
        }
        $result = $db->query("SELECT * FROM 
        (SELECT *,row_number() over (order by kode) as rownumb From $temp_mutasi) T 
        $paging order by satuan,kode asc");
        $query = $db->query("SELECT COUNT(kode) FROM $temp_mutasi");
        $dataq = $query->fetch();
        $jumData = $dataq[0];
    } else {

        $qproc = $db->query("SELECT b2_opname.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan,Sum(b2_opname.d_jumlah) AS t_jumlah From b2_opname left join m_master_barang on b2_opname.d_kode_barang=m_master_barang.kode_barang Where b2_opname.tahun_stok=$th1 And b2_opname.bulan_stok= $bl1 And b2_opname.kategori_input ='$group' and b2_opname.d_kode_barang like '%$kode%' and m_master_barang.nama_barang like '%$nama%' and b2_opname.jenis_data_in_out ='LOCAL' GROUP BY b2_opname.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan order by b2_opname.d_kode_barang asc");
        while ($proc = $qproc->fetch()) {
            $qcek1 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$proc[0]'");
            $cek1 = $qcek1->fetch();
            if ($cek1[0] < 1) {
                $ins1 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc[0]','$proc[1]','$proc[2]',$proc[3],0,0,0,0,0,0)");
            } else {
                $up1 = $db->query("UPDATE $temp_mutasi set saldoa=$proc[3] where kode='$proc[0]'");
            }
        }
        $qproc2 = $db->query("SELECT b2_opname.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan,Sum(b2_opname.d_jumlah) AS t_jumlah From b2_opname left join m_master_barang on b2_opname.d_kode_barang=m_master_barang.kode_barang Where b2_opname.tahun_stok=$th2 And b2_opname.bulan_stok= $bl2 And b2_opname.kategori_input ='$group' and b2_opname.d_kode_barang like '%$kode%' and m_master_barang.nama_barang like '%$nama%' and b2_opname.jenis_data_in_out ='LOCAL' GROUP BY b2_opname.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan order by b2_opname.d_kode_barang asc");
        while ($proc2 = $qproc2->fetch()) {
            $qcek2 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$proc2[0]'");
            $cek2 = $qcek2->fetch();
            if ($cek2[0] < 1) {
                $ins2 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc2[0]','$proc2[1]','$proc2[2]',0,0,0,0,0,0,$proc2[3]) ");
            } else {
                $up2 = $db->query("UPDATE $temp_mutasi set stok=$proc2[3] where kode='$proc2[0]'");
            }
        }
        $qproc3 = $db->query("SELECT b2_adj.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan,Sum(b2_adj.d_jumlah) as t_jumlah,Sum(b2_adj.d_jumlah_2) as t_jumlah_2 From b2_adj left join m_master_barang on b2_adj.d_kode_barang=m_master_barang.kode_barang where b2_adj.kategori_input ='$group' and b2_adj.tgl_mutasi_aktual >='$tgl1' and b2_adj.d_kode_barang like '%$kode%' and m_master_barang.nama_barang like '%$nama%' and b2_adj.tgl_mutasi_aktual <='$tgl2' and b2_adj.jenis_data_in_out ='LOCAL' GROUP BY b2_adj.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan ORDER BY b2_adj.d_kode_barang asc");
        while ($proc3 = $qproc3->fetch()) {
            $qcek3 = $db->query("SELECT count(kode) from $temp_mutasi where kode='$proc3[0]'");
            $cek3 = $qcek3->fetch();
            if ($cek3[0] < 1) {
                $ins3 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc3[0]','$proc3[1]','$proc3[2]',0,0,0,$proc3[3],$proc3[4],0,0)");
            } else {
                $up3 = $db->query("UPDATE $temp_mutasi set adjt=$proc3[3],adjk=$proc3[4] where kode='$proc3[0]'");
            }
        }
        $qproc4 = $db->query("SELECT b2_in.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan, Sum(b2_in.d_jumlah) AS t_jumlah From b2_in left join m_master_barang on b2_in.d_kode_barang=m_master_barang.kode_barang where (b2_in.tgl_mutasi_aktual) between '$tgl1' and '$tgl2' and (b2_in.kategori_input) = '$group' And (b2_in.pabean_tidak) = 'PABEAN' and b2_in.d_kode_barang like '%$kode%' and m_master_barang.nama_barang like '%$nama%' And (b2_in.subcont_tidak) = 'TIDAK' GROUP BY b2_in.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan ORDER BY b2_in.d_kode_barang asc");
        while ($proc4 = $qproc4->fetch()) {
            $qcek4 = $db->query("SELECT count(kode),masuk from $temp_mutasi where kode='$proc4[0]' group by kode,masuk");
            $cek4 = $qcek4->fetch();
            if ($cek4[0] < 1) {
                $ins4 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc4[0]','$proc4[1]','$proc4[2]',0,$proc4[3],0,0,0,0,0)");
            } else {
                $up4 = $db->query("UPDATE $temp_mutasi set masuk=($proc4[3]+$cek4[1]) where kode='$proc4[0]'");
            }
        }
        $qproc41 = $db->query("SELECT b2_in.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan, Sum(b2_in.d_jumlah) AS t_jumlah From b2_in left join m_master_barang on b2_in.d_kode_barang=m_master_barang.kode_barang where (b2_in.tgl_mutasi_aktual) between '$tgl1' and '$tgl2' and (b2_in.kategori_input) = '$group' and b2_in.d_kode_barang like '%$kode%' And m_master_barang.nama_barang like '%$nama%' and (b2_in.local_tidak) = 'LOCAL' GROUP BY b2_in.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan ORDER BY b2_in.d_kode_barang asc");
        while ($proc41 = $qproc41->fetch()) {
            $qcek41 = $db->query("SELECT count(kode),masuk from $temp_mutasi where kode='$proc41[0]' group by kode,masuk");
            $cek41 = $qcek41->fetch();
            if ($cek41[0] < 1) {
                $ins41 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc41[0]','$proc41[1]','$proc41[2]',0,$proc41[3],0,0,0,0,0)");
            } else {
                $up41 = $db->query("UPDATE $temp_mutasi set masuk=($proc41[3]+$cek41[1]) where kode='$proc41[0]'");
            }
        }
        $qproc5 = $db->query("SELECT b2_o.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan, Sum(b2_o.d_jumlah) AS t_jumlah From b2_o left join m_master_barang on b2_o.d_kode_barang=m_master_barang.kode_barang where (b2_o.tgl_mutasi_aktual) between '$tgl1' and '$tgl2' and (b2_o.kategori_input) = '$group' and b2_o.d_kode_barang like '%$kode%' and m_master_barang.nama_barang like '%$nama%' And (b2_o.local_tidak) = 'LOCAL' GROUP BY b2_o.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan ORDER BY b2_o.d_kode_barang asc");
        while ($proc5 = $qproc5->fetch()) {
            $qcek5 = $db->query("SELECT count(kode),keluar from $temp_mutasi where kode='$proc5[0]' group by kode,keluar");
            $cek5 = $qcek5->fetch();
            if ($cek5[0] < 1) {
                $ins5 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc5[0]','$proc5[1]','$proc5[2]',0,0,$proc5[3],0,0,0,0)");
            } else {
                $up5 = $db->query("UPDATE $temp_mutasi set keluar=($proc5[3]+$cek5[1]) where kode='$proc5[0]'");
            }
        }
        $qproc51 = $db->query("SELECT b2_o.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan, Sum(b2_o.d_jumlah) AS t_jumlah From b2_o left join m_master_barang on b2_o.d_kode_barang=m_master_barang.kode_barang where (b2_o.tgl_mutasi_aktual) between '$tgl1' and '$tgl2' and (b2_o.kategori_input) = '$group' and b2_o.d_kode_barang like '%$kode%' and m_master_barang.nama_barang like '%$nama%' And (b2_o.pabean_tidak) = 'PABEAN' And (b2_o.subcont_tidak) = 'TIDAK' GROUP BY b2_o.d_kode_barang,m_master_barang.nama_barang,m_master_barang.satuan ORDER BY b2_o.d_kode_barang asc");
        while ($proc51 = $qproc51->fetch()) {
            $qcek51 = $db->query("SELECT count(kode),keluar from $temp_mutasi where kode='$proc51[0]' group by kode,keluar");
            $cek51 = $qcek51->fetch();
            if ($cek51[0] < 1) {
                $ins51 = $db->query("INSERT INTO $temp_mutasi(kode,nama,satuan,saldoa,masuk,keluar,adjt,adjk,saldob,stok) values('$proc51[0]','$proc51[1]','$proc51[2]',0,0,$proc51[3],0,0,0,0)");
            } else {
                $up51 = $db->query("UPDATE $temp_mutasi set keluar=($proc51[3]+$cek51[1]) where kode='$proc51[0]'");
            }
        }

        $result = $db->query("SELECT * FROM 
        (SELECT *,row_number() over (order by kode) as rownumb From $temp_mutasi) T 
        $paging order by satuan,kode asc");
        $query = $db->query("SELECT COUNT(kode) FROM $temp_mutasi");
        $dataq = $query->fetch();
        $jumData = $dataq[0];
    }
} catch (PDOException $e) {
    print "Koneksi atau query bermasalah: " . $e->getMessage() . "<br/>";
    die();
}
