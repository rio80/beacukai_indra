<?php
class SelectMenu {};

$hr = function ($pg = null, $gr = null) {
 return "menupilih.php?pg=$pg&gr=$gr";
};

$page_link = function (
 $pg = null,
 $kode = null,
 $strKeyword = null,
 $opsi = null,
 $gr = null,
 $hal = null,
 $search = null) {
 return "menupilih.php?pg=$pg&kd=$kode&cr=$strKeyword&op=$opsi&gr=$gr&hal=$hal&search=$search";
};

$link_pabean = function ($obj_pabean) {
    $obj = $obj_pabean;
    $pg = $obj->pg;
    $opsi =$obj->opsi;
    $tgl1 =$obj->tgl1;
    $tgl2 =$obj->tgl2;
    $kode =$obj->kode;
    $jbc =$obj->jbc;
    $nbc =$obj->nbc;
    $hal =$obj->hal;
    $search =$obj->search;

    // print_r($obj);die;
    return "menupilih.php?pg=$pg&kode=$kode&tgl1=$tgl1&tgl2=$tgl2&opsi=$opsi&jbc=$jbc&nbc=$nbc&hal=$hal&search=$search";
   };

$ua_daftar_user = function($pg = null,  $hal = null, $search_username = null){
    return "menupilih.php?pg=$pg&hal=$hal&search_username=$search_username";
};


$mutasi_hari = function ($obj_mutasi){
    $obj = $obj_mutasi;
    $pg = $obj->pg;
    $gr = $obj->gr;
    $opsi =$obj->opsi;
    $kode =$obj->kode;
    $nama =$obj->nama;
    $tgl1 =$obj->tgl1;
    $tgl2 =$obj->tgl2;
    $hal =$obj->hal;
    $search =$obj->search;

    return "menupilih.php?pg=$pg&gr=$gr&tgl1=$tgl1&tgl2=$tgl2&opsi=$opsi&kode=$kode&nama=$nama&hal=$hal&search=$search";
   };

$mutasi_bulan = function ($obj_mutasi){
    $obj = $obj_mutasi;
    $pg = $obj->pg;
    $gr = $obj->gr;
    $opsi =$obj->opsi;
    $kode =$obj->kode;
    $nama =$obj->nama;
    $bulan =$obj->bulan;
    $tahun =$obj->tahun;
    $hal =$obj->hal;
    $search =$obj->search;

    return "menupilih.php?pg=$pg&gr=$gr&bulan=$bulan&tahun=$tahun&opsi=$opsi&kode=$kode&nama=$nama&hal=$hal&search=$search";
   };

$user_log = function ($obj_user_log){
    $obj = $obj_user_log;
    $pg = $obj->pg;
    $usern =$obj->usern;
    $tgl1 =$obj->tgl1;
    $tgl2 =$obj->tgl2;
    $hal =$obj->hal;
    $search =$obj->search;

    return "menupilih.php?pg=$pg&tgl1=$tgl1&tgl2=$tgl2&usern=$usern&hal=$hal&search=$search";
   };

   $hasil_prod = function ($obj_hasil_prod){
    $obj = $obj_hasil_prod;
    $pg = $obj->pg;
    $tgl1 =$obj->tgl1;
    $tgl2 =$obj->tgl2;
    $kode =$obj->kode;
    $nama =$obj->nama;
    $opsi =$obj->opsi;
    $search =$obj->search;
    $hal = $obj->hal;

    return "menupilih.php?pg=$pg&tgl1=$tgl1&tgl2=$tgl2&kode=$kode&nama=$nama&opsi=$opsi&hal=$hal&search=$search";
   };
?>