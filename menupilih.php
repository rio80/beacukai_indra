<?php
#error_reporting("E_ALL ^ E_NOTICE");
$page = $_GET['pg'];
include_once 'backend/index.php';

switch ($page) {
    #default;
    case "100";
        include 'index.php';
        break;

    case "101";
        include "auth/login.php";
        break;

    case "102";
        include "auth/logout.php";
        break;

    case "103";
        include "functions.php";
        break;

    // case "0"; include "pages/index.php"; break;
    // case "01"; include "pages/info.php"; break;

    case "200";
        include "pages/pabean_masuk.php";
        break;

    case "200-view";
        return
            $view('pabean_masuk/pabean_masuk_view.php', "Info Pabean Masuk");
        break;

    case "200-pdf";
        include "pages/pabean_masuk_pdf.php";
        break;

    case "200-excel";
        include "pages/pabean_masuk_excel.php";
        break;

    case "200-print";
        include "pages/pabean_masuk_print.php";
        break;

    case "300";
        include "pages/pabean_keluar.php";
        break;

    case "300-view";
        return
            $view('pabean_keluar/pabean_keluar_view.php', "Info Pabean Keluar");
        break;

    case "300-pdf";
        include "pages/pabean_keluar_pdf.php";
        break;

    case "300-excel";
        include "pages/pabean_keluar_excel.php";
        break;

    case "300-print";
        include "pages/pabean_keluar_print.php";
        break;

    case "400";
        include "pages/pass.php";
        break;

    case "401";
        include "pages/user_add.php";
        break;

    case "401-add";
        include "pages/user_save.php";
        break;

    case "401-edit";
        include "pages/user_view.php";
        break;

    case "401-save";
        include "pages/user_update.php";
        break;

    case "401-del";
        include "pages/user_delete.php";
        break;

    case "500";
        include "pages/mutasi_barang.php";
        break;

    case "500-view";
        return $view('mutasi_harian/mutasi_view.php', "Mutasi Harian");
        break;

    case "500-pdf";
        include "pages/mutasi_barang_pdf.php";
        break;

    case "500-excel";
        return $view('mutasi_harian/mutasi_excel.php', "Mutasi Excel");
        break;

    case "500-print";
        include "pages/mutasi_barang_print.php";
        break;

    case "510";
        include "pages/mutasi_barang.php";
        break;

    case "510-view";
        return $view('mutasi_bulanan/mutasi_view.php', "Mutasi Bulanan");
        break;

    case "510-pdf";
        include "pages/mutasi_barang_pdf.php";
        break;

    case "510-excel";
        return $view('mutasi_bulanan/mutasi_excel.php', "Mutasi Excel");
        break;

    case "510-print";
        include "pages/mutasi_barang_print.php";
        break;

    case "610";
        return
        include "pages/barang/barang_search.php";
        break;
    // case "610-view"; include "pages/barang_view.php"; break;
    case "610-view";
        return
            $view('barang/barang_view.php', "Info Barang");
        break;

    case "610-pdf";
        include "pages/barang_pdf.php";
        break;

    case "610-excel";
        $view('barang/barang_excel.php', null);
        break;

    case "610-print";
        include "pages/barang_print.php";
        break;

    case "700";
        include "pages/hasil_produksi.php";
        break;

    case "700-view";
        return
            $view('hasil_produksi/hasil_view.php', "Hasil Produksi");
        break;

    case "700-pdf";
        include "pages/hasil_produksi_pdf.php";
        break;

    case "700-excel";
        return
            $view('hasil_produksi/hasil_excel.php', "Hasil Produksi");
        break;

    case "700-print";
        include "pages/hasil_produksi_print.php";
        break;

    case "ua-pabean-msk";
        include "pages/ua_pabean_masuk.php";
        break;

    case "ua-pabean-msk-view";
        include "pages/ua_pabean_masuk_view.php";
        break;

    case "ua-pabean-msk-excel";
        include "pages/ua_pabean_masuk_excel.php";
        break;

    case "ua-pabean-klr";
        include "pages/ua_pabean_keluar.php";
        break;

    case "ua-pabean-klr-view";
        include "pages/ua_pabean_keluar_view.php";
        break;

    case "ua-mutasi";
        include "pages/ua_mutasi.php";
        break;

    case "ua-mutasi-view";
        include "pages/ua_mutasi_view.php";
        break;

    case "ua-mutasi2";
        include "pages/ua_mutasi2.php";
        break;

    case "ua-mutasi-view2";
        include "pages/ua_mutasi_view2.php";
        break;

    case "ua-mutasi-hp";
        include "pages/ua_mutasi_hp.php";
        break;

    case "ua-mutasi-hp-view";
        include "pages/ua_mutasi_hp_view.php";
        break;

    case "ua-daftar-user";
        return $view('user_activity/daftar_user/daftar_user_view.php', "Daftar User");
        break;

    case "manual_report";
        return $view('manual_doc/manual_report.php');
        break;

    case "manual_cctv";
        return $view('manual_doc/manual_cctv.php');
        break;

    case "ua-daftar-log";
        return $view('user_activity/user_log/user_log_view.php');
        break;

    case "4";
        include "logout.php";
        break;
}
