<?php
$root = "../../../";
include_once $root . 'backend/bg-ua-daftar-user.php';
include_once $root . 'backend/select_menu.php';

$table_th = [
    'No', 'Username', 'Dept', 'Hak Input', 'Hak Edit',
    'Hak Hapus', 'Hak Lihat Web', 'Menu Mutasi FG', 'Menu Mutasi Material', 'Menu Mutasi Modal',
    'Menu Mutasi Penolong', 'Menu Mutasi Consumable', 'Menu Mutasi Scrap',
    'Menu Pabean Masuk', 'Menu Pabean Keluar', 'Menu Produksi'];

echo "Daftar User dan Hak";

?>

<div class="table-responsive" style="padding: 20px 15px;">
    <table class="table table-bordered table-sm">
        <thead>
            <tr style="background-color:#e3e8e8">
            <?php
                foreach ($table_th as $value) {
                echo "<th scope='col'>" . $value . "</th>";
                }
                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                $no = 1;
                while ($value = $result->fetch()) {
                echo "<tr>";
                echo "<td align='center'>" . (($no++) + (($noPage - 1) * $dataPerPage)) . "</td>";

                for ($i = 0; $i < count($table_th) - 1; $i++) {
                echo "<td scope='row'>" . $value[$i] . "</td>";
                }
                echo "</tr>";
                }

            ?>
            <div class="row">
                <div class="col-sm-6">
                    <?php include 'pagination.php';?>
                </div>
                <div class="col-sm-6 text-right">

                    <p>
                        <input type="text" name="search_username" id="search_username">
                        <!-- <a href="pages/pabean_masuk/pabean_masuk_excel.php?pg=200-excel&op=<?php echo $opsi; ?>&kdx=<?php echo $kode; ?>&nbc=<?php echo $nbc; ?>&jbc=<?php echo $jbc; ?>&tgl1=<?php echo $tgl1; ?>&tgl2=<?php echo $tgl2; ?>&setpage=0"
                            target="blank" class="btn btn-icon btn-success">
                            <i class="fa fa-file-excel"></i> Excel
                        </a> -->

                    </p>
                </div>
            </div>
        </tbody>
    </table>
    <?php include 'pagination.php';?>

    <script>
        $(function () {

            let el = '#search_username';
            $(el).keypress(function(e){
                if(e.which == 13){
                    let username = $(el).val();
                    $.ajax({
                        url: 'pages/user_activity/daftar_user/daftar_user_table.php',
                        type: 'GET',
                        data: {
                            search_username : username,
                            },
                        beforeSend: function(){
                        },
                        success: function (result) {
                            $('.card').html(result);
                            $(el).val(username);
                            $(el).focus();
                        },
                        error: function(result){
                            console.log(result);
                            
                        }
                    })
                }
            })  
        })
       
    </script>