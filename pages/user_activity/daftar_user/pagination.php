      <!--
      -- Buat Paginationnya
      -- Dengan bootstrap, kita jadi dimudahkan untuk membuat tombol-tombol pagination dengan design yang bagus tentunya
      -->
      <style>
          .active{
            background-color: #6777ef;
            color: #fff;
            border-color: transparent;
          }
      </style>
      <ul class="pagination">
        <!-- LINK FIRST AND PREV -->
        <?php
        require_once "../../../backend/select_menu.php";
  

        if($noPage == 1){ // Jika page adalah page ke 1, maka disable link PREV
        ?>
          <li class="page-link disabled"><a href="#">First</a></li>
          <li class="page-link disabled"><a href="#">&laquo;</a></li>
        <?php
        }else{ // Jika page bukan page ke 1
          $link_prev = ($noPage > 1)? $noPage - 1 : 1;
        ?>
          <li>
              <a class="page-link" href="
              <?php 
              echo $ua_daftar_user("ua-daftar-user", 1); 
              ?>
              ">First</a></li>
          <li>
              <a class="page-link" href="
              <?php echo $ua_daftar_user("ua-daftar-user", $link_prev, $search_username); ?>
              ">&laquo;</a></li>
        <?php
        }
        ?>
        
        <!-- LINK NUMBER -->
        <?php
        
        $jumlah_page = ceil($jumData / $dataPerPage); // Hitung jumlah halamannya

        $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif

        $start_number = ($noPage > $jumlah_number)
        ? $noPage - $jumlah_number 
        : 1; // Untuk awal link number

        $end_number = ($noPage < ($jumlah_page - $jumlah_number))
        ? $noPage + $jumlah_number 
        : $jumlah_page; // Untuk akhir link number
        for($i = $start_number; $i <= $end_number; $i++){
            
          $link_active = ($noPage == $i)? ' class="active"' : '';
          $href_active = ($noPage == $i)? 'active' : '';
          echo "<li". $link_active .">
          <a class='page-link $href_active' href='".$ua_daftar_user("ua-daftar-user", $i, $search_username)."'>$i</a>
          </li>";

        }

        ?>
        
        <!-- LINK NEXT AND LAST -->
        <?php
        // Jika page sama dengan jumlah page, maka disable link NEXT nya
        // Artinya page tersebut adalah page terakhir 
        if($noPage == $jumlah_page){ // Jika page terakhir
        ?>
          <li class="page-link disabled"><a href="#">&raquo;</a></li>
          <li class="page-link disabled"><a href="#">Last</a></li>
        <?php
        }else{ // Jika Bukan page terakhir
          $link_next = ($noPage < $jumlah_page)? $noPage + 1 : $jumlah_page;
        ?>
          <li>
              <a class='page-link' href="
              <?php echo $ua_daftar_user("ua-daftar-user", $link_next, $search_username); ?>
              ">&raquo;</a>
            </li>

          <li>
              <a class='page-link' href="
              <?php echo $ua_daftar_user("ua-daftar-user", $jumlah_page, $search_username); ?>
              ">Last</a>
        </li>
        <?php
        }
        ?>
      </ul>