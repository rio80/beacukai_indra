
<script>
    $().ready(function(){
        var hal = "<?php echo (isset($_GET['hal']) ? $_GET['hal'] : ''); ?>";
        var search = "<?php echo (isset($_GET['search']) ? $_GET['search'] : ''); ?>";
        var search_username = "<?php echo (isset($_GET['search_username']) ? $_GET['search_username'] : ''); ?>";
        $('#loading').hide();
       
        $.ajax({
            url : 'pages/user_activity/daftar_user/daftar_user_table.php',
            type: 'GET',
            data: {
                hal :hal,
                search :search,
                search_username :search_username,
            },
            beforeSend: function(){
                $('#loading').append('<br><br><h3>Mohon Tunggu..</h3>').show();
            },
            error: function(result){
                if(result.status === 404) $('#loading').load("./errors/errors-404.html");  
                if(result.status === 500) $('#loading').load("./errors/errors-500.html"); 
            },
            
            success: function(result){
                $('#loading').html('');
                $('.card').html(result);
            }
        });
 
    })

</script>
