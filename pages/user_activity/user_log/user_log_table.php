<?php
$root = '../../../';
require_once $root.'backend/bg-user-log.php';
include_once $root.'backend/select_menu.php';
include_once 'user_log_search.php';
$table_th = ['No', 'Tanggal', 'Nama User', 'Menu.Form.Akses', 'Aktifitas'];
?>
<div class="text-center"><span><h4>Jumlah Data : <?php echo $jumData ?></h4></span></div>

<div align="center">
    <?php
        echo "Periode ".$da1." s/d ".$da2;
        echo "<br>Terdapat ".number_format($jumData)." Record Log";
    ?>
    <input type="hidden" id="title" value="<?php echo 'PT.Standart Indonesia Industri - Log Periode : '.$da1.' s/d '.$da2; ?>">
    <input type="hidden" id="file" value="<?php echo 'Log : '.$da3.' s/d '.$da4; ?>">
</div>

<div class="table-responsive" style="padding: 20px 15px;">
    <table class="table table-bordered table-sm">
        <thead>
            <tr style="background-color:#e3e8e8">
                <?php
foreach ($table_th as $value) {
    echo "<th scope='col'>" . $value . "</th>";
}
?>
            </tr>
        </thead>
        <tbody>
<?php

$no = 1;
while ($data=$qproc->fetch()){
    ?>                           
        <tr>
            <?php 
                echo "<td align='center'>" . (($no++) + (($noPage - 1) * $dataPerPage)) . "</td>";
            ?>
            <td><?php #echo date_format(date_create($data[0]),'d-M-Y h:i');
                    echo date_format(date_create($data[0]),'d-M-Y'); ?></td>
            <td><?php echo $data[1];?></td>
            <td><?php echo $data[2];?></td>
            <td><?php echo $data[3];?></td>
        </tr>

        <?php
    }
    ?>

            <div class="row">
                <div class="col-sm-6">
                    <?php include 'pagination.php';?>
                </div>
               
                <div class="col-sm-6 text-right">

                   <?php
                        if($_GET['search'] == 1){
                    ?>
                    <a href=<?php echo $hr('ua-daftar-log') ?>  
                    class="btn btn-icon btn-info" 
                    id="reset_search">
                        Reset Search
                    </a>
                    <?php
                        }
                    ?>
                    
                    <a href=# class="btn btn-icon btn-primary" id="cari_user_log">
                        <i class="fa fa-search"></i> Cari
                    </a>
                    <a href="pages/user_activity/user_log/user_log_excel.php?pg=ua-daftar-log&tgl1=<?php echo $tgl1; ?>&tgl2=<?php echo $tgl2; ?>&usern=<?php echo $_SESSION['usern'] ?>&setpage=0" target="_blank" class="btn btn-icon btn-success">

                    <!-- <a href="menupilih.php?pg=610-excel&op=<?php echo $opsi; ?>&kd=<?php echo $kode; ?>&cr=<?php echo $strKeyword; ?>&gr=<?php echo $_GET['gr'];?>" class="btn btn-icon btn-success"> -->

                        <i class="fa fa-file-excel"></i> Excel
                    </a>
                </div>
            </div>
        </tbody>
    </table>
    <?php include 'pagination.php';?>
    <script>

        $().ready(function () {
            $("#cari_user_log").fireModal({
                title: 'Cari Daftar User Log',
                body: $('#search_user_log'),
                center: true,
            });
        });
    </script>
</div>