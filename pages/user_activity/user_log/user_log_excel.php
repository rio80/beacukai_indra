<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);


include_once '../../../backend/bg-user-log.php';

$table_th = ['No', 'Tanggal', 'Nama User', 'Menu.Form.Akses', 'Aktifitas'];

session_start();
$user = $_SESSION['userbc'];
$qcdata=$db->query("SELECT nama_user,level,web_bisa_lihat from user_password where nama_user='$user'");
    
    
    $cdatax=$qcdata->fetch();
    $qper=$db->query("SELECT * from pt");
    $per=$qper->fetch();
    $pt=$per[1];
    
    $level=$cdatax[1];

    
$tanggal=date("Ymd");
ob_start();
if (headers_sent()) {
    echo headers_sent();
    echo "<br>";
    die("Redirect failed. Please click on this link: <a href=...>");
}
else{
    header('Content-type: application/ms-excel');
    header("Content-Disposition: attachment; filename=User Log - ".$tanggal.".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}

?>

<p><b><?php echo $pt; ?></b><br>
    Laporan User Log  : <?php echo $usern . " ". $tgl1." s/d ".$tgl2; ?></p>
    <table class="table" border="1">
        <thead>
            <tr>
            <?php
                foreach ($table_th as $value) {
                    echo "<th scope='col'>" . $value . "</th>";
                }
                ?>
            </tr>
          
        </thead>
        <tbody style="font-size: 12px">
           
                <?php
                    $no = 1;
                    while ($data=$qproc->fetch()){
                        ?>                           
                            <tr>
                                <?php 
                                    echo "<td align='center'>" . $no++ . "</td>";
                                ?>
                                <td><?php echo date_format(date_create($data[0]),'d-M-Y h:i');?></td>
                                <td><?php echo $data[1];?></td>
                                <td><?php echo $data[2];?></td>
                                <td><?php echo $data[3];?></td>
                            </tr>
                    
                            <?php
                        }
                ?>
            </tr>
            <?php
                $db = null;
          
            ?>
        </tbody>
    </table>