<?php
include_once '../../backend/bg-mutasi-harian.php';
include_once '../../backend/select_menu.php';

$table_th = [
 'No', 'Kode', 'Nama', 'Satuan', 'Saldo Awal',
 'Pemasukan', 'Pengeluaran', 'Penyesuaian', 'Saldo Buku',
 'Stok Opname', 'Selisih', 'Keterangan'];

 echo '<span style="font-size: 22px">'.$model .' Harian</span>';
?>

<div class="text-center">
<span style="font-size: 16px"><b>Periode : </b><?php $tg1 = explode("-", $tgl1); echo $tg1[2]. "-" .date('F', mktime(0,0,0, $tg1[1], 10)). "-".$tg1[0] ?>  <b>Sampai  </b><?php $tg2 = explode("-", $tgl2); echo $tg2[2]. "-" .date('F', mktime(0,0,0, $tg2[1], 10)). "-".$tg2[0] ?></span>
<br> <span style="font-size: 16px">Jumlah Data : <?php echo $jumData ?></span>
</div>
<div class="table-responsive" style="padding: 20px 15px;">
<?php if(isset($pesan_bulanan)) 
{
    echo "<div class='text-center' style='font-size:20px; 
    font-weight:bold; background-color:green; color:white'>".$pesan_bulanan."</div>";
}

    include_once 'mutasi_search.php';

    ?>
    <table class="table table-bordered table-sm">


        <thead>
            <tr style="background-color:#e3e8e8">
                <?php
foreach ($table_th as $value) {
 echo "<th scope='col'>" . $value . "</th>";
}
?>
            </tr>
        </thead>

        <tbody>
            <?php
$no = 1;
while ($data = $result->fetch()) {
echo "<tr>";
echo "<td align='center'>" . (($no++) + (($noPage - 1) * $dataPerPage)) . "</td>"; ?>
<?php
     for ($i=0; $i < count($table_th) -1; $i++) { 

        if($i == 4 || $i == 5 || $i == 6){
            echo "<td scope='row' class='text-right'>" . number_format($data[$i]). "</td>";
        }else{
            echo "<td scope='row'>" . $data[$i] . "</td>";

        }
    }

echo " </tr>";
} 
?>


            <div class="row">
                <div class="col-sm-6">
                    <?php include 'pagination.php';?>
                </div>
                <div class="col-sm-6 text-right">
                    <?php
if ($_GET['search'] == 1) {
 ?>
                    <a href=<?php echo $hr('500-view', $gr) ?> class="btn btn-icon btn-info" id="reset_search">
                        Reset Search
                    </a>
                    <?php
}
?>

                    <a href="#" class="btn btn-icon btn-primary" id="cari_mutasi_hari">
                        <i class="fa fa-search"></i> Cari
                    </a>


                    <a href="pages/mutasi_harian/mutasi_excel.php?pg=500-excel&op=<?php echo $opsi; ?>&tgl1=<?php echo $tgl1; ?>&tgl2=<?php echo $tgl2; ?>&kode=<?php echo $kode; ?>&nama=<?php echo $nama; ?>&gr=<?php echo $_GET['gr'];?>&user=<?php echo $_SESSION['userbc'] ?>&setpage=0" target="_blank" class="btn btn-icon btn-success">
                    <i class="fa fa-file-excel"></i> Excel
                    </a>
                </div>
            </div>
        </tbody>
    </table>
    <?php include 'pagination.php';?>
    <script>

        $().ready(function () {
            $("#cari_mutasi_hari").fireModal({
                title: 'Cari Mutasi Harian',
                body: $('#search_mutasi_hari'),
                center: true,
            });
        });
    </script>
</div>

<script>
    $(function(){
        $('input:radio').change(function(){
            let jenis = $("form input[type='radio']:checked").val();
            if(jenis === '0'){
                $('#kode').val('');
                $('#nama').val('');
            }
            if(jenis === '1'){
                $('#nama').val('');
            }
            if(jenis === '2'){
                $('#kode').val('');
            } 
        })
    });
</script>