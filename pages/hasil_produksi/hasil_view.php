
<script>
    $().ready(function(){
       
        $('#loading').hide();
        var opsi = "<?php echo (isset($_POST['opsi']) ? $_POST['opsi'] : '')?>";
        var kode = "<?php echo (isset($_POST['kode']) ? $_POST['kode'] : '')?>";
        var nama = "<?php echo (isset($_POST['nama']) ? $_POST['nama'] : '')?>";
        var tgl1 = "<?php echo (isset($_POST['tgl1']) ? $_POST['tgl1'] : '')?>";
        var tgl2 = "<?php echo (isset($_POST['tgl2']) ? $_POST['tgl2'] : '')?>";
        var search = "<?php echo (isset($_POST['search']) ? $_POST['search'] : '')?>";
        
        if(search == ''){
            search = "<?php echo (isset($_GET['search']) ? $_GET['search'] : '')?>";
        }
      
        if(opsi == ''){
            opsi = "<?php echo (isset($_GET['opsi']) ? $_GET['opsi'] : '')?>";
        }

        if(kode == ''){
            kode = "<?php echo (isset($_GET['kode']) ? $_GET['kode'] : '')?>";
        }

        if(nama == ''){
            nama = "<?php echo (isset($_GET['nama']) ? $_GET['nama'] : '')?>";
        }

        if(tgl1 == ''){
            tgl1 = "<?php echo (isset($_GET['tgl1']) ? $_GET['tgl1'] : '')?>";
        }

        if(tgl2 == ''){
            tgl2 = "<?php echo (isset($_GET['tgl2']) ? $_GET['tgl2'] : '')?>";
        }
        // console.log(window.localStorage.getItem('search'));
        
        var hal = "<?php echo (isset($_GET['hal']) ? $_GET['hal'] : ''); ?>";
        
        $.ajax({
            url : 'pages/hasil_produksi/hasil_table.php',
            type: 'GET',
            data: {
                hal: hal,
                opsi: opsi,
                kode: kode,
                nama: nama,
                tgl1: tgl1,
                tgl2: tgl2,
                search: search,
            },
            beforeSend: function(){
                $('#loading').append('<br><br><h3>Mohon Tunggu..</h3>').show();
            },
            error: function(result){
                if(result.status === 404) $('#loading').load("./errors/errors-404.html");  
                if(result.status === 500) $('#loading').load("./errors/errors-500.html"); 
            },
            
            success: function(result){
                $('#loading').html('');
                $('.card').html(result);
            }
        });
 
    })

</script>
