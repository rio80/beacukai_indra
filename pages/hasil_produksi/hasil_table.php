<?php
include_once '../../backend/bg-hasil-produksi.php';
include_once '../../backend/select_menu.php';
include_once 'hasil_search.php';

$table_th = ['No', 'Tanggal','Kode Barang', 'Nama Barang', 'Satuan', 'Proses WIP', 'Lokasi Area Barang', 'Qty'];
echo '<span style="font-size: 22px">'.$title.'</span>';
session_start();
?>

<div class="text-center">
    <?php
        if (!empty($tgl1) || !empty($tgl2)) {
    ?>
    <span style="font-size: 14px"><b>Periode : </b><?php $tg1 = explode("-", $tgl1); echo $tg1[2]. "-" .date('F', mktime(0,0,0, $tg1[1], 10)). "-".$tg1[0] ?>  <b>Sampai  </b><?php $tg2 = explode("-", $tgl2); echo $tg2[2]. "-" .date('F', mktime(0,0,0, $tg2[1], 10)). "-".$tg2[0] ?></span>
    <?php } ?>

    <br><span style="font-size: 16px">Jumlah Data : <?php echo $jumData ?></span>
</div>
<div class="table-responsive" style="padding: 20px 15px;">
    <table class="table table-bordered table-sm">
        <thead>
            <tr style="background-color:#e3e8e8">
                <?php
foreach ($table_th as $value) {
    echo "<th scope='col'>" . $value . "</th>";
}
?>
            </tr>
        </thead>
        <tbody>
<?php
$no = 1;
while ($value = $result->fetch()) {
    echo "<tr>";
    echo "<td align='center'>" . (($no++) + (($noPage - 1) * $dataPerPage)) . "</td>";
 
    for ($i=0; $i < count($table_th) -1; $i++) { 
        if($i == 6){
            echo "<td scope='row' class='text-right'>" . $value[$i] . "</td>";

        }else{
            echo "<td scope='row'>" . $value[$i] . "</td>";

        }
    }
    
    echo "</tr>";

}

?>

            <div class="row">
                <div class="col-sm-6">
                    <?php include 'pagination.php';?>
                </div>
               
                <div class="col-sm-6 text-right">

                   <?php
                        if($_GET['search'] == 1){
                    ?>
                    <a href=<?php echo $hr('700-view') ?>  
                    class="btn btn-icon btn-info" 
                    id="reset_search">
                        Reset Search
                    </a>
                    <?php
                        }
                    ?>
                    
                    <a href=# class="btn btn-icon btn-primary" id="cari_hasil">
                        <i class="fa fa-search"></i> Cari
                    </a>
                    <a href="pages/hasil_produksi/hasil_excel.php?pg=700-excel&opsi=<?php echo $opsi; ?>&tgl1=<?php echo $tgl1; ?>&tgl2=<?php echo $tgl2; ?>&kode=<?php echo $_GET['kode'];?>&nama=<?php echo $_GET['nama'] ?>&setpage=0" target="_blank" class="btn btn-icon btn-success">

                    <!-- <a href="menupilih.php?pg=610-excel&op=<?php echo $opsi; ?>&kd=<?php echo $kode; ?>&cr=<?php echo $strKeyword; ?>&gr=<?php echo $_GET['gr'];?>" class="btn btn-icon btn-success"> -->

                        <i class="fa fa-file-excel"></i> Excel
                    </a>
                </div>
            </div>
        </tbody>
    </table>
    <?php include 'pagination.php';?>
    <script>

        $().ready(function () {
            $("#cari_hasil").fireModal({
                title: 'Cari Hasil Produksi',
                body: $('#search_hasil'),
                center: true,
            });
        });
    </script>
</div>