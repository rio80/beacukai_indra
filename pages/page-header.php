<?php

include_once 'backend/koneksi.php';
include_once 'backend/select_menu.php'; // var : $hr
ini_set('display_errors',1);
error_reporting(E_ALL & ~E_NOTICE);
if (!isset($_SESSION['userbc'])) {
  header("location:menupilih.php?pg=101");
}
$nick = $_SESSION['userbc'];

$crtbl=$db->query("IF NOT EXISTS (SELECT * from sysobjects where name='cctv' and xtype='U') CREATE TABLE cctv(cctv_lihat varchar(100) not null,cctv_tools varchar(100) not null)");
        $qcctv=$db->query("SELECT * from cctv");
        $cctv=$qcctv->fetch();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>mySYSTEM Inventory</title>
  
<?php include_once 'assets/assets-css.php' ?>
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>
<style>

</style>
<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
          
        </form>
        <ul class="navbar-nav navbar-right">
          
          
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, <?php echo $nick ?></div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <!-- <div class="dropdown-title">Logged in 5 min ago</div> -->
              <!-- <a href="features-profile.html" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <a href="features-activities.html" class="dropdown-item has-icon">
                <i class="fas fa-bolt"></i> Activities
              </a>
              <a href="features-settings.html" class="dropdown-item has-icon">
                <i class="fas fa-cog"></i> Settings
              </a> -->
              <div class="dropdown-divider"></div>
              <a href="menupilih.php?pg=102" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="menupilih.php?pg=100">Menu Utama</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <!-- <a href="index.html">St</a> -->
          </div>
          <ul class="sidebar-menu">
           
            <li class="dropdown">
              <a href="menupilih.php?pg=100" class="nav-link"><i class="fas fa-columns"></i> <span>Info</span></a>
             
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Data Barang</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?php echo $hr('610-view', '0') ?>">Barang Jadi</a></li>
                <li><a class="nav-link" href="<?php echo $hr('610-view', '1') ?>">Bahan Baku</a></li>
                <li><a class="nav-link" href="<?php echo $hr('610-view', '2') ?>">Bahan Penolong</a></li>
                <li><a class="nav-link" href="<?php echo $hr('610-view', '3') ?>">Bahan Consumable</a></li>
                <li><a class="nav-link" href="<?php echo $hr('610-view', '4') ?>">Barang Modal</a></li>
                <li><a class="nav-link" href="<?php echo $hr('610-view', '5') ?>">Barang Scrap</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href=<?php echo $hr('200-view') ?> class="nav-link"><i class="fas fa-th-large"></i> <span>Pabean Masuk</span></a>
             
            </li>
            <li class="dropdown">
              <a href=<?php echo $hr('300-view')?> class="nav-link"><i class="far fa-file-alt"></i> <span>Pabean Keluar</span></a>
              
            </li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-map-marker-alt"></i> <span>mutasi Harian</span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo $hr('500-view', '0') ?>">Barang Jadi</a></li>
                <li><a href="<?php echo $hr('500-view', '1') ?>">Bahan Baku</a></li>
                <li><a href="<?php echo $hr('500-view', '2') ?>">Bahan Penolong</a></li>
                <li><a href="<?php echo $hr('500-view', '3') ?>">Barang Consumable</a></li>
                <li><a href="<?php echo $hr('500-view', '4') ?>">Barang Modal</a></li>
                <li><a href="<?php echo $hr('500-view', '5') ?>">Barang Scrap</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-map-marker-alt"></i> <span>mutasi Bulanan</span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo $hr('510-view', '0') ?>">Barang Jadi</a></li>
                <li><a href="<?php echo $hr('510-view', '1') ?>">Bahan Baku</a></li>
                <li><a href="<?php echo $hr('510-view', '2') ?>">Bahan Penolong</a></li>
                <li><a href="<?php echo $hr('510-view', '3') ?>">Barang Consumable</a></li>
                <li><a href="<?php echo $hr('510-view', '4') ?>">Barang Modal</a></li>
                <li><a href="<?php echo $hr('510-view', '5') ?>">Barang Scrap</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="<?php echo $hr('700-view') ?>" class="nav-link"><i class="fas fa-plug"></i> <span>Hasil produksi</span></a>
              
            </li>
            <!-- <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>User Activities</span></a>
              <ul class="dropdown-menu">
                <li><a href=<?php echo $hr('ua-daftar-user') ?>>Daftar User & Hak</a></li> 
                <li><a href=<?php echo $hr('ua-daftar-log') ?>>Daftar User Log</a></li> 
                <li><a href="auth-register.html">Pabean Masuk</a></li> 
                <li><a href="auth-reset-password.html">Pabean Keluar</a></li> 
                <li><a href="auth-reset-password.html">Mutasi Bahan Baku</a></li> 
                <li><a href="auth-reset-password.html">Mutasi Bahan Penolong</a></li> 
                <li><a href="auth-reset-password.html">Mutasi Modal</a></li> 
                <li><a href="auth-reset-password.html">Mutasi Scrap</a></li> 
                <li><a href="auth-reset-password.html">Barang Hasil Produksi</a></li> 
                <li><a href="auth-reset-password.html">Mutasi Barang Jadi</a></li> 
              </ul>
            </li> -->
           
            <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Manual</span></a>
              <ul class="dropdown-menu">
                
              <li>
                <a href="assets/file/MANUAL SYSTEM REPORT INVENTORY BEACUKAI.pdf" target="_blank"><span>Manual Report</span></a>
                <!-- <a href="<?php echo $hr('manual_report') ?>" target="_blank"><span>Manual Report</span></a> -->
              <li>
                <a href="assets/file/MANUAL CCTV.pdf" target="_blank"><span>Manual CCTV</span></a>
                <!-- <a href="<?php echo $hr('manual_cctv') ?>" target="_blank"><span>Manual CCTV</span></a> -->
              </ul>
            </li>
            <!-- <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>CCTV</span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo $cctv[1];?>" target="_blank">Download Driver</a></li> 
                <li><a href="<?php echo $cctv[0];?>" target="_blank">Lihat CCTV</a></li> 
               
              </ul>
            </li> -->
            <!-- <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Konfigurasi</span></a>
              <ul class="dropdown-menu">
                <li><a href="auth-forgot-password.html">Sandi</a></li> 
               
              </ul>
            </li> -->
            <li class="dropdown">
              <a href="menupilih.php?pg=102" class="nav-link"><i class="far fa-user"></i> <span>Exit System</span></a>
              
            </li>
          </ul>

        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <!-- <div class="section-header"> -->
            <h4><?php echo (isset($_SESSION['section_header']) ? $_SESSION['section_header'] : '') ?></h4>
          <!-- </div> -->