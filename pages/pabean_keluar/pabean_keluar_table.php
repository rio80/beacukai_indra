<?php
include_once '../../backend/bg-pabean-keluar.php';
include_once '../../backend/select_menu.php';
include_once 'pabean_keluar_search.php';

$table_th = [
    'No', 'Jenis Doc', 'No. BC', 'Tanggal Doc', 'No. Bukti', 
    'Tanggal Bukti', 'Penerima', 'Kode Barang', 'Nama Barang', 'Unit', 
    'Jumlah', 'M.U', 'Nilai Barang'];

    echo '<span style="font-size: 22px">'.$title .'</span>';
?>

<div class="text-center">
<span style="font-size: 16px"><b>Periode : </b><?php $tg1 = explode("-", $tgl1); echo $tg1[2]. "-" .date('F', mktime(0,0,0, $tg1[1], 10)). "-".$tg1[0] ?>  <b>Sampai  </b><?php $tg2 = explode("-", $tgl2); echo $tg2[2]. "-" .date('F', mktime(0,0,0, $tg2[1], 10)). "-".$tg2[0] ?></span>
<br><span style="font-size: 16px">Jumlah Data : <?php echo $jumData ?></span>
</div>
<div class="table-responsive" style="padding: 20px 15px;">
    <table class="table table-bordered table-sm">
        <thead>
            <tr style="background-color:#e3e8e8">
                <?php
                    foreach ($table_th as $value) {
                        echo "<th scope='col'>" . $value . "</th>";
                    }
                ?>
            </tr>
        </thead>
        <tbody>
        <?php
            $no = 1;
            while ($value = $result->fetch()) {
                echo "<tr>";
                echo "<td align='center'>" . (($no++) + (($noPage - 1) * $dataPerPage)) . "</td>";
            
                for ($i=0; $i < count($table_th) -1; $i++) { 
                    if($i == 2 || $i == 4){
                        echo "<td scope='row' width='100px'>" . $value[$i] . "</td>";
                    }else if ($i == 9 || $i == 11) {
                        if($value[$i] <= 0){
                            $value[$i] = 0;
                        }
                        echo "<td scope='row' width='60px' class='text-right'>" .  number_format($value[$i], 2) . "</td>";
                    }
                    else{
                        echo "<td scope='row'>" . $value[$i] . "</td>";
                    }
                }
                
                echo "</tr>";

            }

        ?>

            <div class="row">
                <div class="col-sm-6">
                    <?php include 'pagination.php';?>
                </div>
                <div class="col-sm-6 text-right">
                <?php
                        if($_GET['search'] == 1){
                    ?>
                    <a href=<?php echo $hr('200-view') ?>  
                    class="btn btn-icon btn-info" 
                    id="reset_search">
                        Reset Search
                    </a>
                    <?php
                        }
                    ?>

                     <a href="#" class="btn btn-icon btn-primary" id="cari_pabean_keluar">
                        <i class="fa fa-search"></i> Cari
                    </a>
           
                    
                    <a href="pages/pabean_keluar/pabean_keluar_excel.php?pg=300-excel&op=<?php echo $opsi; ?>&kdx=<?php echo $kode; ?>&nbc=<?php echo $nbc; ?>&jbc=<?php echo $jbc; ?>&tgl1=<?php echo $tgl1;?>&tgl2=<?php echo $tgl2;?>&setpage=0" target="blank" class="btn btn-icon btn-success">
                        <i class="fa fa-file-excel"></i> Excel
                    </a>
                </div>
            </div>
        </tbody>
    </table>
    <?php include 'pagination.php';?>
    <script>

        $().ready(function () {
            $("#cari_pabean_keluar").fireModal({
                title: 'Cari Pabean Keluar',
                body: $('#search_pabean_keluar'),
                center: true,
            });
        });
    </script>
</div>