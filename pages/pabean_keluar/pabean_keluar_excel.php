<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);


include_once '../../backend/bg-pabean-keluar.php';
session_start();

$user = $_SESSION['userbc'];
$qcdata=$db->query("SELECT nama_user,level,web_bisa_lihat from user_password where nama_user='$user'");
    
    
    $cdatax=$qcdata->fetch();
    $qper=$db->query("SELECT * from pt");
    $per=$qper->fetch();
    $pt=$per[1];
    
    $level=$cdatax[1];

    
$tanggal=date("Ymd");
ob_start();
if (headers_sent()) {
    echo headers_sent();
    echo "<br>";
    die("Redirect failed. Please click on this link: <a href=...>");
}
else{
    header('Content-type: application/ms-excel');
    header("Content-Disposition: attachment; filename=Pabean Keluar-".$tanggal.".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
}

?>

<p><b><?php echo $pt; ?></b><br>
    Laporan Pengeluaran Barang per Dokumen Pabean : <?php echo $tgl1." s/d ".$tgl2; ?></p>
    <table class="table" border="1">
        <thead>
            <tr>
                <th rowspan="2">No</th>
                <th align="center" colspan="3">Dokumen Pabean</th>
                <th align="center" colspan="2">Bukti Penerimaan barang</th>
                <th rowspan="2">Penerima</th>
                <th rowspan="2">Kode Barang</th>
                <th rowspan="2">Nama Barang</th>
                <th rowspan="2">Unit</th>
                <th rowspan="2">Jumlah</th>
                <th rowspan="2">M.U</th>
                <th rowspan="2">Nilai Barang</th>
            </tr>
            <tr>
                <th>Jenis Doc</th>
                <th>No.BC</th>
                <th>Tanggal Doc</th>
                <th>No. Bukti</th>
                <th>Tanggal Bukti</th>
            </tr>
        </thead>
        <tbody style="font-size: 12px">
            <?php
                $no=1;
                while
                    ($data=$result->fetch()){
            ?>
            <tr>
                <td align="center"><?php echo ($no++);?></td>
                <td class="text"><?php echo $data[0];?></td>
                <td class="text"><?php echo $data[1];?></td>
                <td>
                    <?php 
                        $t=date_create($data[2]);
                        echo date_format($t,"d-m-Y");
                    ?>
                </td>
                <td class="text"><?php echo $data[3];?></td>
                <td>
                    <?php 
                        $t=date_create($data[4]);
                        echo date_format($t,"d-m-Y");
                    ?>
                </td>
                <td><?php echo $data[5];?></td>
                <td><?php echo $data[6];?></td>
                <td><?php echo $data[7];?></td>
                <td><?php echo $data[8];?></td>
                <td><?php echo number_format($data[9]);?></td>
                <td><?php echo $data[10];?></td>
                <td><?php echo number_format($data[11]);?></td>
            </tr>
            <?php
                }
                $db = null;
          
            ?>
        </tbody>
    </table>