<div id="search_pabean_keluar">
    <div class="grid_3 grid_4">
        <h3>Laporan Pabean Keluar</h3>
        <div>
            <div class="form-group" align="center">
                <form method="POST" action="menupilih.php?pg=300-view">
                    <fieldset>
                        <table>
                            <tr>
                                <td>Tanggal</td>
                                <td> : </td>
                                <td>
                                <input name="search" value="1" type="hidden">
                                    <input name="tgl1" type="date" placeholder="Dari Tanggal"
                                        class="form-control datepicker" style="width: 200px" required="yes">
                                </td>
                                <td>
                                    <input name="tgl2" type="date" placeholder="Sampai Tanggal"
                                        class="form-control datepicker" style="width: 200px" required="yes">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="radio block"><label><input name="opsi" type="radio" checked=""
                                                value="0"> Semua Data</label></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="radio block"><label><input name="opsi" type="radio" value="1"> Cari Kode
                                            Barang</label></div>
                                </td>
                                <td> : </td>
                                <td colspan="2"><input name="kode" class="form-control" placeholder="Kode Barang"
                                        type="text"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="radio block"><label><input name="opsi" type="radio" value="2"> Cari
                                            Nomer BC</label></div>
                                </td>
                                <td> : </td>
                                <td colspan="2">
                                    <input name="nomer" class="form-control" placeholder="No BC" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="radio block"><label><input name="opsi" type="radio" value="3"> By Jenis
                                            BC</label></div>
                                </td>
                                <td> : </td>
                                <td colspan="2">
                                    <select name="jenis" class="form-control">
                                        <option selected="">-- Silahkan Pilih --</option>
                                        <?php 
                                            $qcjenis=$db->query("SELECT * FROM m_bc_jenis WHERE in_out='OUT'");
                                            while($cjenis=$qcjenis->fetch()){
                                        ?>
                                        <option value="<?php echo $cjenis[1];?>"><?php echo $cjenis[1]; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right"><button type="submit" class="btn btn-success"><span
                                            class="fa fa-search"> Cari</span></button></td>
                            </tr>
                        </table>
                    </fieldset>
                </form>
                </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="clearfix"> </div>