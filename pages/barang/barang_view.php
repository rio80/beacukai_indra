
<script>
    $().ready(function(){
       
        $('#loading').hide();
        var opsi = "<?php echo (isset($_POST['opsi']) ? $_POST['opsi'] : '')?>";
        var kode = "<?php echo (isset($_POST['kode']) ? $_POST['kode'] : '')?>";
        var nama = "<?php echo (isset($_POST['nama']) ? $_POST['nama'] : '')?>";
        var search = "<?php echo (isset($_POST['search']) ? $_POST['search'] : '')?>";
        
        if(search == ''){
            search = "<?php echo (isset($_GET['search']) ? $_GET['search'] : '')?>";
        }
      
        if(opsi == ''){
            opsi = "<?php echo (isset($_GET['opsi']) ? $_GET['opsi'] : '')?>";
        }
        if(opsi == ''){
            opsi = "<?php echo (isset($_GET['op']) ? $_GET['op'] : '')?>";
        }
        if(kode == ''){
            kode = "<?php echo (isset($_GET['kode']) ? $_GET['kode'] : '')?>";
        }
        if(kode == ''){
            kode = "<?php echo (isset($_GET['kd']) ? $_GET['kd'] : '')?>";
        }
        if(nama == ''){
            nama = "<?php echo (isset($_GET['nama']) ? $_GET['nama'] : '')?>";
        }
        if(nama == ''){
            nama = "<?php echo (isset($_GET['cr']) ? $_GET['cr'] : '')?>";
        }
        // console.log(window.localStorage.getItem('search'));
        
        var gr = "<?php echo (isset($_GET['gr']) ? $_GET['gr'] : ''); ?>";
        var hal = "<?php echo (isset($_GET['hal']) ? $_GET['hal'] : ''); ?>";
        
        $.ajax({
            url : 'pages/barang/barang_table.php',
            type: 'GET',
            data: {
                gr: gr,
                hal: hal,
                opsi: opsi,
                kode: kode,
                nama: nama,
                search: search,
            },
            beforeSend: function(){
                $('#loading').append('<br><br><h3>Mohon Tunggu..</h3>').show();
            },
            error: function(result){
                if(result.status === 404) $('#loading').load("./errors/errors-404.html");  
                if(result.status === 500) $('#loading').load("./errors/errors-500.html"); 
            },
            
            success: function(result){
                $('#loading').html('');
                $('.card').html(result);
            }
        });
 
    })

</script>
