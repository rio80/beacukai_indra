<?php
include_once '../../backend/bg-barang.php';
include_once '../../backend/select_menu.php';
include_once 'barang_search.php';

$table_th = ['No', 'Kode Barang', 'Nama Barang', 'Keterangan', 'Satuan', 'Status'];
echo '<span style="font-size: 22px">'.$title.'</span>';
session_start();
?>
<div class="text-center">
<span style="font-size: 16px">Jumlah Data : <?php echo $jumData ?></span>
</div>
<div class="table-responsive" style="padding: 20px 15px;">
    <table class="table table-bordered table-sm">
        <thead>
            <tr style="background-color:#e3e8e8">
                <?php
foreach ($table_th as $value) {
    echo "<th scope='col'>" . $value . "</th>";
}
?>
            </tr>
        </thead>
        <tbody>
<?php
$no = 1;
while ($value = $result->fetch()) {
    echo "<tr>";
    echo "<td align='center'>" . (($no++) + (($noPage - 1) * $dataPerPage)) . "</td>";
 
    for ($i=0; $i < count($table_th) -1; $i++) { 
        echo "<td scope='row'>" . $value[$i] . "</td>";
    }
    
    echo "</tr>";

}

?>

            <div class="row">
                <div class="col-sm-6">
                    <?php include 'pagination.php';?>
                </div>
               
                <div class="col-sm-6 text-right">

                   <?php
                        if($_GET['search'] == 1){
                    ?>
                    <a href=<?php echo $hr('610-view', $_GET['gr']) ?>  
                    class="btn btn-icon btn-info" 
                    id="reset_search">
                        Reset Search
                    </a>
                    <?php
                        }
                    ?>
                    
                    <a href=# class="btn btn-icon btn-primary" id="cari_barang">
                        <i class="fa fa-search"></i> Cari
                    </a>
                    <a href="pages/barang/barang_excel.php?pg=610-excel&op=<?php echo $opsi; ?>&kd=<?php echo $kode; ?>&cr=<?php echo $strKeyword; ?>&gr=<?php echo $_GET['gr'];?>&user=<?php echo $_SESSION['userbc'] ?>&setpage=0" target="_blank" class="btn btn-icon btn-success">

                    <!-- <a href="menupilih.php?pg=610-excel&op=<?php echo $opsi; ?>&kd=<?php echo $kode; ?>&cr=<?php echo $strKeyword; ?>&gr=<?php echo $_GET['gr'];?>" class="btn btn-icon btn-success"> -->

                        <i class="fa fa-file-excel"></i> Excel
                    </a>
                </div>
            </div>
        </tbody>
    </table>
    <?php include 'pagination.php';?>
    <script>

        $().ready(function () {
            $("#cari_barang").fireModal({
                title: 'Cari Barang',
                body: $('#search_barang'),
                center: true,
            });
        });
    </script>
</div>