<?php
include_once '../../backend/bg-mutasi-bulan.php';
include_once '../../backend/select_menu.php';
include_once 'mutasi_search.php';

$table_th = [
 'No', 'Kode', 'Nama', 'Satuan', 'Saldo Awal',
 'Pemasukan', 'Pengeluaran', 'Penyesuaian', 'Saldo Buku',
 'Stok Opname', 'Selisih', 'Keterangan'];

 echo '<span style="font-size: 22px">'.$title .' per Bulan</span>';
?>

<div class="text-center"><span>
        <?php if(!empty($search)) { ?>
        <span><h6>Periode : <?php echo date('F', mktime(0,0,0, $bulan, 10)) . ' - '. $tahun ?></h6></span>
        <?php } ?>
        <span style="font-size: 16px">Jumlah Data : <?php echo $jumData ?></span>
    </span></div>


<div class="table-responsive" style="padding: 20px 15px;">
    <table class="table table-bordered table-sm">


        <thead>
            <tr style="background-color:#e3e8e8">
                <?php
foreach ($table_th as $value) {
 echo "<th scope='col'>" . $value . "</th>";
}
?>
            </tr>
        </thead>

        <tbody>
            <?php
$no = 1;
while ($value = $result->fetch()) {
    echo "<tr>";
    echo "<td align='center'>" . (($no++) + (($noPage - 1) * $dataPerPage)) . "</td>";
 
    for ($i=0; $i < count($table_th) -1; $i++) { 
        if($i == 3 || $i == 4 || $i == 5 || $i == 6 || $i == 7 || $i == 8 || $i == 9){
            echo "<td scope='row' class='text-right'>" . number_format($value[$i], 2). "</td>";
        }else{
            echo "<td scope='row'>" . $value[$i] . "</td>";

        }
    }
    
    echo "</tr>";

}
 ?>



            <div class="row">
                <div class="col-sm-6">
                    <?php include 'pagination.php';?>
                </div>
                <div class="col-sm-6 text-right">
                    <?php
if ($_GET['search'] == 1) {
 ?>
                    <a href=<?php echo $hr('510-view', $gr) ?> class="btn btn-icon btn-info" id="reset_search">
                        Reset Search
                    </a>
                    <?php
}
?>

                    <a href="#" class="btn btn-icon btn-primary" id="cari_mutasi_bulan">
                        <i class="fa fa-search"></i> Cari
                    </a>


                    <a href="pages/mutasi_bulanan/mutasi_excel.php?pg=500-excel&op=<?php echo $opsi; ?>&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun; ?>&kd=<?php echo $kode; ?>&nama=<?php echo $nama; ?>&gr=<?php echo $_GET['gr'];?>&setpage=0" target="_blank" class="btn btn-icon btn-success">
                    <i class="fa fa-file-excel"></i> Excel
                    </a>
                </div>
            </div>
        </tbody>
    </table>
    <?php include 'pagination.php';?>
    <script>

        $().ready(function () {
            $("#cari_mutasi_bulan").fireModal({
                title: 'Cari Mutasi Bulanan',
                body: $('#search_mutasi_bulan'),
                center: true,
            });
        });
    </script>
</div>

<script>
    $(function(){
        $('input:radio').change(function(){
            let jenis = $("form input[type='radio']:checked").val();
            if(jenis === '0'){
                $('#kode').val('');
                $('#nama').val('');
            }
            if(jenis === '1'){
                $('#nama').val('');
            }
            if(jenis === '2'){
                $('#kode').val('');
            } 
        })
    });
</script>