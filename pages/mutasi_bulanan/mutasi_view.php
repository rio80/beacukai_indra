
<script>
    $().ready(function(){
        $('#loading').hide();
        var opsi = "<?php echo (isset($_POST['opsi']) ? $_POST['opsi'] : '')?>";
        var kode = "<?php echo (isset($_POST['kode']) ? $_POST['kode'] : '')?>";
        var nama = "<?php echo (isset($_POST['nama']) ? $_POST['nama'] : '')?>";
        var bulan = "<?php echo (isset($_POST['bulan']) ? $_POST['bulan'] : '')?>";
        var tahun = "<?php echo (isset($_POST['tahun']) ? $_POST['tahun'] : '')?>";
        var hal = "<?php echo (isset($_GET['hal']) ? $_GET['hal'] : ''); ?>";
        var search = "<?php echo (isset($_POST['search']) ? $_POST['search'] : '')?>";
        if(search == ''){
            search = "<?php echo (isset($_GET['search']) ? $_GET['search'] : '')?>";
        }
        if(opsi == ''){
            opsi = "<?php echo (isset($_GET['opsi']) ? $_GET['opsi'] : '')?>";
        }
    
        if(bulan == ''){
            bulan = "<?php echo (isset($_GET['bulan']) ? $_GET['bulan'] : '')?>";
        }
        
        if(tahun == ''){
            tahun = "<?php echo (isset($_GET['tahun']) ? $_GET['tahun'] : '')?>";
        }
       
        if(kode == ''){
            kode = "<?php echo (isset($_GET['kode']) ? $_GET['kode'] : '')?>";
        }
        if(nama == ''){
            nama = "<?php echo (isset($_GET['nama']) ? $_GET['nama'] : '')?>";
        }
        var gr = "<?php echo (isset($_GET['gr']) ? $_GET['gr'] : ''); ?>";
        console.log(kode);
        
        $.ajax({
            url : 'pages/mutasi_bulanan/mutasi_table.php',
            type: 'GET',
            
            data: {
                gr: gr,
                opsi: opsi,
                kode: kode,
                nama: nama,
                bulan: bulan,
                tahun: tahun,
                hal:hal,
                search:search,
            },
            beforeSend: function(){

                $('#loading').append('<br><br><h3>Mohon Tunggu..</h3>').show();
            },
            error: function(result){

                if(result.status === 404) $('#loading').load("./errors/errors-404.html");  
                if(result.status === 500) $('#loading').load("./errors/errors-500.html"); 
            },
            
            success: function(result){

                $('#loading').html('');
                $('.card').html(result);
            }
        });
 
    })

</script>
