<?php
	
	set_time_limit(0);
	
	ini_set('display_errors',1);
	session_start();
	header('Refresh:1800');
	function login_validate() {
		$timeout = 1800;
		$_SESSION["expires_by"] = time() + $timeout;
	}

	function login_check() {
		$exp_time = $_SESSION["expires_by"];
		if (time() < $exp_time) {
		login_validate();
		return true;
		} else {
		unset($_SESSION["expires_by"]);
		return false;
		}
	}
?>