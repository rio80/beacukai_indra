<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Login &mdash; mySYSTEM</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/modules/fontawesome/css/all.min.css">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="assets/modules/bootstrap-social/bootstrap-social.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style-modified.css">
  <link rel="stylesheet" href="assets/css/components.css">
  <!-- Start GA -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());

    gtag('config', 'UA-94034622-3');
  </script>
  <!-- /END GA -->
  <style>
    html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,dl,dt,dd,ol,nav ul,nav li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;vertical-align:baseline;}
    .card-primary {
      /* width: 520px;
      height: 450px; */
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
      border: 4px solid;
      padding: 30px 40px 5px 40px;
    }
    img{max-width:100%;}

    body {

      background: url('assets/img/banner.jpg')no-repeat;
      background-size: cover;
      padding: 70px 0px 70px 0px;
      min-height: 300px;
    }
    body {
      min-height: 800px;
    }

    @media(max-width:480px){
      body {
        min-height: 700px;
      }
    }
  </style>
</head>
<?php
$info = json_decode(json_encode(parse_ini_file('assets/img/style.jpg')));

?>
<body>
  <div class="card card-primary">
    <div class="card-header">
      <div class="col-md-12 text-center">
        <img src="assets/img/ptlogo.png" alt="" srcset="" class="text-center" width="80" height="80"><br>

        <h4 class="text-center">Login
          <?php echo $info->nama_perusahaan ?>
        </h4>
      </div><br>

    </div>

    <div class="card-body">
      <!-- <form method="POST" action="backend/bg-login.php" class="needs-validation" novalidate=""> -->
      <form method="POST" action="backend/bg-login.php">
        <div class="form-group">
          <!-- <label for="username">Username</label> -->
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
            </div>
            <!-- <input id="userbc" type="text" class="form-control" name="userbc" tabindex="1"
              placeholder="User Name" required autofocus> -->
              <input id="userbc" type="text" class="form-control" name="userbc" tabindex="1"
              placeholder="User Name" autofocus>
          </div>

          <div class="invalid-feedback">
            Please fill in your Username
          </div>
        </div>

        <div class="form-group">
          <div class="d-block">
            <!-- <label for="password" class="control-label">Password</label> -->

          </div>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
            </div>
            <!-- <input id="passbc" type="password" class="form-control" name="passbc" tabindex="2" placeholder="Password" required> -->
            <input id="passbc" type="password" class="form-control" name="passbc" tabindex="2" placeholder="Password">
          </div>
          <div class="invalid-feedback">
            please fill in your password
          </div>
        </div>


        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4" style="font-size: 16px;">
            Login
          </button>
        </div>
        <div class="text-center">
        (1)Lihat CCTV -->Gunakan INTERNET EXPLORER (IE)<br>
        (2)Lihat laporan inventory -->Gunakan google CHROME
</div>

      </form>


    </div><br>
    <div class="text-center"> Copyright &copy; mySYSTEM</div>

  </div>



  <!-- General JS Scripts -->
  <script src="assets/modules/jquery.min.js"></script>
  <script src="assets/modules/popper.js"></script>
  <script src="assets/modules/tooltip.js"></script>
  <script src="assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="assets/modules/moment.min.js"></script>
  <script src="assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Page Specific JS File -->

  <!-- Template JS File -->
  <script src="assets/js/scripts.js"></script>
  <script src="assets/js/custom.js"></script>
</body>

</html>